# Explorations in Free Software GIS 

This is a collection of projects / case studies using free software for
geographic information systems applications.

It is hosted on the web through the
[Open Book Project](http://www.openbookproject.net/tutorials/explore_fsgis).
