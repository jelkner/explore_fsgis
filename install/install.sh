#!/bin/bash

# Update the machine
apt update
apt dist-upgrade -y

# Setup Database
apt install postgresql-9.5-postgis-2.2 -y
cp files/pg_hba.conf /etc/postgresql/9.5/main/
cp files/postgresql.conf /etc/postgresql/9.5/main/
service postgresql restart
su - postgres -c "createuser --superuser fsgis"
su - postgres -c "psql -c \"ALTER ROLE fsgis PASSWORD 'fsgis'\""
su - fsgis -c "createdb map_arlington"
su - fsgis -c "psql -d map_arlington -c 'CREATE EXTENSION postgis'"

# Populate the data base with Arlington data
aptitude install osmosis -y
aptitude install imposm -y

if ! [ -f virginia-latest.osm.pbf ]
then
    wget http://download.geofabrik.de/north-america/us/virginia-latest.osm.pbf
fi
if ! [ -f arlington.osm.pbf ]
then
    osmosis --read-pbf file=virginia-latest.osm.pbf --bounding-box top=38.9342803955 left=-77.1723251343 bottom=38.8272895813 right=-77.032081604 --write-pbf file=arlington.osm.pbf
fi
imposm --read arlington.osm.pbf
imposm --write --connection postgis://fsgis:fsgis@localhost/map_arlington

# Clean up the table names and drop genN tables
source ./rename_tables.sh

# Install Tilestache and Mapnik with Matt Gallagher's Docker containter
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
cp files/docker.list /etc/apt/sources.list.d/
aptitude install docker-engine -y
