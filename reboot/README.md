# 2023/24 Reboot

Herein I will document the reboot of this project over 5 years after it was
last touched.  I have a group of interested students and an exciting potential
real world application project motivating my return to GIS study.

I'll take the dry run notes here in markdown, and then update the turial once
I figure out what changes need to be made to it.


## Will it still work?

I began with a fresh install of Debian 12, on which I ran:
```
$ sudo apt install postgis
```

Postgres 10 is now Postgres 15, but the file location of
``/etc/postgresql/15/main/pg_hba/conf`` is where I expected it.

I changed the line
```
host    all             all             127.0.0.1/32            scram-sha-256
```
to
```
host    all             all             0.0.0.0/0               scram-sha-256
```
and then in ``/etc/postgresql/15/main/postgresql.conf`` changed:
```
#listen_addresses = 'localhost'
```
to
```
listen_addresses = '*'
```
Give my user superuser instructions remain unchanged.

## Database this time
```
$ createdb map_va
$ psql -d map_va -c 'CREATE EXTENTION postgis'
```

## Connect to VM from host
```
$ psql -h 192.168.122.32 -p 5432 -U jelkner -d map_va
```

## Things Have *Really* Changed! Getting OSM Data into PostGIS

Back in 2017 I used a tool called [imposm](https://imposm.org/) to load
[OpenStreetMap](https://en.wikipedia.org/wiki/OpenStreetMap) data into PostGIS.

There was a debian package for it and setup was just an ``apt install``. It's
not there today, and it looks like the Python version of imposm hasn't been
updated since 2015.

A quick websearch revealed that the current tool for this seems to be
[osm2pgsql](https://osm2pgsql.org/). There *is* a debian package in the repo
for this, and I installed it with:
```
$ sudo apt install osm2pgsql
```
Loading the data was just a matter of running:
```
$ osm2pgsql virginia-latest.osm.pbf -d map_va
```
Viewing the loaded data with QGIS:

![OSM Virginia Roads in QGIS](images/qgis_with_map_va_roads.png)

I'll now update the tutorial and begin looking into loading more specific data.


## Building this tutorial

To build this tutorial, I needed to run:
```
$ sudo apt install python3-sphinx texlive texlive-latex-extra dvipng
```
