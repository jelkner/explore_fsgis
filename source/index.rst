Explorations in Free Software GIS
=================================

.. image:: _static/osmlogo.png
   :class: osm 

by Jeffrey Elkner 

Last updated: 5 November 2023


Purpose of this Collection 
--------------------------

This collection is a presentation of several small case studies using free
software to explore GIS. 

The Collection
--------------


Map Arlington
~~~~~~~~~~~~~

* `Extracting OSM Data for a Localized Map Server <map_arlington1.html>`__
* `Importing Shapefile Data into PostGIS <map_arlington2.html>`__
* `Setting Up TileStache and Mapnik with Docker <map_arlington3.html>`__
* `Adding a JavaScript Front End with Leaflet <map_arlington4.html>`__


Map Virginia 
~~~~~~~~~~~~

* `Extracting OSM Data for a Localized Map Server Revisited <map_va1.html>`__
* `VA Votes Project <map_va2.html>`__


Voting and Income in Arlington
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* `Setting Up the Postgis Database <vote_arlington1.html>`__
* `Populating the Data <vote_arlington2.html>`__
* `Processing the Data <vote_arlington3.html>`__
* `Analysing the Data <vote_arlington4.html>`__
* `Extending the Analysis <vote_arlington5.html>`__


NOVA Plays
~~~~~~~~~~

* `Moving a PostgreSQL Database to SpatiaLite <nova_plays1.html>`__


To be continued...
~~~~~~~~~~~~~~~~~~

* `Harvesting Twitter Data with Python <tweet_harvest.html>`__


.. toctree::
   :maxdepth: 1
   :hidden:

   copyright.rst
   map_arlington1.rst
   map_arlington2.rst
   map_arlington3.rst
   map_arlington4.rst
   map_va1.rst
   map_va2.rst
   vote_arlington1.rst
   vote_arlington2.rst
   vote_arlington3.rst
   vote_arlington4.rst
   vote_arlington5.rst
   nova_plays1.rst
   tweet_harvest.rst
   fdl-1.3.rst


Reference
---------

* `Copyright Notice <copyright.html>`__
* `GNU Free Document License <fdl-1.3.html>`__
* :ref:`genindex`
* :ref:`search`
