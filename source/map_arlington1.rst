..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _making_arl_map_1:

Map Arlington 1: Extracting OSM Data for a Localized Map Server
===============================================================

.. index:: OpenStreetMap, OSM, Planet.osm, PBF format 


The Problem
-----------

`OpenStreetMap <https://www.openstreetmap.org>`__ (OSM) is collaborative
map of the world, freely available to anyone to use and contribute to its
development. `Planet.osm <http://wiki.openstreetmap.org/wiki/Planet.osm>`__
has information on downloading the database, which at the time I'm writing this
was 71.4GB for the data in `PBF format
<http://wiki.openstreetmap.org/wiki/PBF_Format>`__.

I wanted to create a map server for my local community of `Arlington County,
Virginia <https://en.wikipedia.org/wiki/Arlington_County,_Virginia>`__. In the
first part of this case study I'll document how I got OSM map data into
PostGIS.


.. index:: superuser, postgresql user, postgis extension

Creating the Database
---------------------

I'm working on an `Debian 12 (Bookworm)
<https://en.wikipedia.org/wiki/Debian_version_history>`__ server
with
`PostgreSQL <https://en.wikipedia.org/wiki/PostgreSQL>`__ 15 and
`PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__ 3.3 installed. These tools
can be installed with the following command::

    $ sudo apt install postgis

To allow external connections to the database, I edited
``/etc/postgresql/15/main/pg_hba.conf`` and changed::

    host    all            all            127.0.0.1/32            scram-sha-256 

to::

    host    all             all           0.0.0.0/0               scram-sha-256

and then edited ``/etc/postgresql/15/main/postgresql.conf`` and changed::

    #listen_addresses = 'localhost'

to::

    listen_addresses = '*'

and then finally ran the following command to restart the database server and
have these changes take effect::

    $ sudo service postgresql restart

Next I gave my user database *superuser* privileges and set its postgress
password with::

    $ sudo su - postgres
    $ createuser --superuser [user] 
    $ psql -c "ALTER ROLE [user] PASSWORD '[password]'"
    $ exit

so I could create a database with::

    $ createdb map_arlington
    $ psql -d map_arlington -c 'CREATE EXTENSION postgis'


I installed this postgres server on a
`KVM <https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine>`__ using
`virt-manager <https://en.wikipedia.org/wiki/Virtual_Machine_Manager>`__. On
the ``192.168.122.x`` network that virt-manager creates, this server had
address ``192.168.122.19``.  To verify I could connect to it I ran::

    $ psql -h 192.168.122.19 -p 5432 -U jelkner -d map_arlington

When greeted with::

   map_arlington=#

I knew I was ready to go.

With a `spatial database <https://en.wikipedia.org/wiki/Spatial_database>`__
created, now it's time to get the data.


.. index:: Geofabrik, osmosis

Getting the Data
----------------

The German company `Geofabrik <http://www.geofabrik.de/>`__ provides a mirror
of Planet.osm data broken down into localized parts, including countries and
states in the U.S..  Their `Virginia page
<http://download.geofabrik.de/north-america/us/virginia.html>`__ has a link to
215 MB file (at the time of this writing) containing recent data for the state
of Virginia.

I downloaded this with::

    $ wget http://download.geofabrik.de/north-america/us/virginia-latest.osm.pbf

While 215 MB isn't unmanageable, for this project I'm only interested in the
small part of that data that contains Arlington County.
`Osmosis <http://wiki.openstreetmap.org/wiki/Osmosis>`__ is a command line
Java application that can extract data from a PBD file within a specified
`bounding box <http://wiki.openstreetmap.org/wiki/Bounding_Box>`__.

To find the bounding box for Arlington County, I used an web application
appropriately named BoundingBox at http://boundingbox.klokantech.com.

With the bounding box determined, I installed ``osmosis`` and used it to
the PBF data for Arlington County::

    $ sudo apt install osmosis
    $ osmosis --read-pbf file=virginia-latest.osm.pbf --bounding-box top=38.9342803955 left=-77.1723251343 bottom=38.8272895813 right=-77.032081604 --write-pbf file=arlington.osm.pbf

17193 milliseconds later I had a 6.6 MB file with Arlington County PBF data.

The next step is loading this data into the PostGIS database.


Loading the PBF Data into PostGIS
---------------------------------

To import the PBF data into the PostGIS database, use `osm2pgsql
<http://`https://osm2pgsql.org>`_, which can be installed with::

    $ sudo apt install osm2pgsql 

After installing osm2pgsql, loading the data into PostGIS can be done with:: 

    $ osm2pgsql arlington.osm.pbf -d map_arlington


Viewing the Database in QGIS
----------------------------

`QGIS <https://en.wikipedia.org/wiki/QGIS>`__ provides a quick and easy way
to view the spatial data in a PostGIS database.  After loading the data with
imposm, I connected QGIS to it:

.. image:: illustrations/map_arlington1/qgis_connect_to_map_arlington.png
   :alt: Map Arlington database in QGIS
   :scale: 80 %

and took a look:

.. image:: illustrations/map_arlington1/map_arl_in_qgis.png
   :alt: Map Arlington database in QGIS

.. note::

    The ``osm_new_roads`` layer reported an error when I tried to load it. This
    didn't happen when I originally wrote this a few years ago.
