..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _making_va_map_1:

Map Virginia 1: Extracting OSM Data for a Localized Map Server Revisited
========================================================================

.. index:: OpenStreetMap, OSM, Planet.osm, PBF format 


The Problem 
-----------

In the 2016 U.S. general election Hillary Clinton received 1,981,473 votes to
Donald Trump's 1,769,443 votes, winning the state [#f1]_. The Democratic party
has won each of the last three presidential contests, yet despite getting the
majority of the votes statewide, Democrats control only four of the eleven 
congressional seats in what Wikipedia describes as "one of the most
gerrymandered states in the country" [#f2]_. It had previously been only three
of the eleven seats, but a court ordered redistricting this past year led to
a gain of one seat by the Democrats. The Virginia House of Delegates is
equally skewed, with Republicans controlling 66 of 100 seats and 21 of 40 seats
in the lower and upper houses respectively [#f3]_.

The goal of this project will be to see what contribution a `civic hacking
<https://en.wikipedia.org/wiki/Civic_technology#Civic_hacking>`__ group can
make to help to counteract this anti-democratic reality. As described in
David Daley's book,
`Rat F**ked: The True Story Behind the Secret Plan to Steal America's Democracy
<http://books.wwnorton.com/books/Ratfked>`__, insightful Republican operatives
used a combination of political and
`geospatial <https://en.wikipedia.org/wiki/Geospatial_analysis>`__ analysis to
scientifically control the rules of the U.S. electoral game, while the mostly
ignorant Democrats got taken by surprise and beaten badly, despite their
numerical superiority.

As the U.S. becomes increasingly unequal and our democracy continues to erode,
any chance of saving it will require the collaborative efforts of large numbers
of us who constitute the vast majority of "the people" yet who lack the
financial resources to compete with those in power using the rules they
create. Can we leverage collaborative effort and shared resources to
effectively fight back? Let's see.


The Plan
--------

The goal will be to use data and geospatial analysis tools to help empower
democratic forces in Virginia to more effectively participate in the electoral
process. Since these tools and techniques will need to be collaborative to be
effective, only free software and open data will be utilized.


.. index:: superuser, postgresql user, postgis extension

Creating the Database
---------------------

A databased named `map_virginia` can be created using the same process
described in :ref:`making_arl_map_1`. The step using osmosis to extract a
part of the Virginia data can be skipped, since we are interested here in the
entire state.


Viewing the Database in QGIS
----------------------------

The
`PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__ data can be viewd in
`QGIS <https://en.wikipedia.org/wiki/QGIS>`__ by creating a connection to
the database from QGIS:

.. image:: illustrations/map_va1/connect_qgis2postgis.png
   :alt: Connecting QGIS to PostGIS database

and then browsing the layers:

.. image:: illustrations/map_va1/map_va_in_qgis.png
   :alt: Map Virginia database in QGIS

The next task will be to begin assembling the data needed to reason about
voters and elections.


.. rubric:: Footnotes

.. [#f1] `Virginia.gov 2016 November General Official Results
   <http://results.elections.virginia.gov/vaelections/2016%20November%20General/Site/Presidential.html>`__. Retrieved from Virginia.gov 12/11/2016.

.. [#f2] `United States presidential elections in Virginia
   <https://en.wikipedia.org/wiki/United_States_presidential_elections_in_Virginia>`__. Retrieved from Wikipedia 12/11/2016.

.. [#f3] `Virginia General Assembly <https://ballotpedia.org/Virginia_General_Assembly>`__. Retrieved from Ballotpedia.or 12/11/2016.
