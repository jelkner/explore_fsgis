..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _making_va_map_2:

Map Virginia 2: VA Votes Project 
================================


The Problem 
-----------

We want to make Virginia election data as accessible as possible for folks
working to increase voter participation. The ability to query a database
holding election results for the past decade or more and to compare results
from year to year, including years with national elections and years with only
local elections, would be most helpful.


The Plan
--------

The `voter precinct <https://en.wikipedia.org/wiki/Precinct>`__ is the atomic
unit for electoral districts in the United States. By storing election results
in a database by precinct, it should be possible to create a wide range of
database queries about the elections.


.. index:: shapefiles, importing shapefiles

Creating the Database and Adding Voter Precincts
------------------------------------------------

Assuming a
`PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__ database setup as in
described in :ref:`making_arl_map_1`, create a database named ``va_votes``
with::

    $ createdb va_votes 
    $ psql -d va_votes -c 'CREATE EXTENSION postgis'

`Shapefiles <https://en.wikipedia.org/wiki/Shapefile>`__ for
`voter precincts <https://en.wikipedia.org/wiki/Precinct>`__ are available
in a github repository at:

    `https://github.com/vapublicaccessproject/va-precinct-maps-2016
    <https://github.com/vapublicaccessproject/va-precinct-maps-2016>`__

which can be downloaded with::

    $ git clone https://github.com/vapublicaccessproject/va-precinct-maps-2016

After changing to the ``va-precinct-maps-2016/shp`` directory and using
`PostGIS 2.0 pgsql2shp shp2pgsql Command Line Cheatsheet
<http://www.bostongis.com/pgsql2shp_shp2pgsql_quickguide.bqg>`__ as a guide,
I ran::

    $ shp2pgsql -s 3857 vaprecincts2016 public.vaprecincts2016 > vaprecincts2016.sql
    $ psql -d va_votes < vaprecincts2016.sql


Viewing the Database in QGIS
----------------------------

The
`PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__ data can be viewed in
`QGIS <https://en.wikipedia.org/wiki/QGIS>`__ by creating a connection to
the database from QGIS:

.. image:: illustrations/map_va2/connect_qgis2postgis.png
   :alt: Connecting QGIS to PostGIS database
   :height: 453px

and then browsing the layers in the *Browser Panel*. There is only one layer
in this case, ``vaprecincts2016``. Double clicking on ``vaprecincts2016``
loads the layer into the *Layers Panel* and displays it:

.. image:: illustrations/map_va2/va_votes_in_qgis.png
   :alt: VA Votes database in QGIS

The next task will be to begin assembling the data needed to reason about
voters and elections.

.. rubric:: Footnotes
