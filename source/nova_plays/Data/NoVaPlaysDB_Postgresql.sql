--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: gist_geometry_ops; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY public.gist_geometry_ops USING gist;


ALTER OPERATOR FAMILY public.gist_geometry_ops USING gist OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aliases; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.aliases (
    id integer NOT NULL,
    playground_id integer,
    aliasname character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.aliases OWNER TO alexandriaplays;

--
-- Name: aliases_id_seq; Type: SEQUENCE; Schema: public; Owner: alexandriaplays
--

CREATE SEQUENCE public.aliases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aliases_id_seq OWNER TO alexandriaplays;

--
-- Name: aliases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alexandriaplays
--

ALTER SEQUENCE public.aliases_id_seq OWNED BY public.aliases.id;


--
-- Name: criteriakeys; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.criteriakeys (
    id integer NOT NULL,
    criterianame character varying(255),
    scalevalue integer,
    textvalue text,
    description character varying(255)
);


ALTER TABLE public.criteriakeys OWNER TO alexandriaplays;

--
-- Name: criteriakeys_id_seq; Type: SEQUENCE; Schema: public; Owner: alexandriaplays
--

CREATE SEQUENCE public.criteriakeys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.criteriakeys_id_seq OWNER TO alexandriaplays;

--
-- Name: criteriakeys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alexandriaplays
--

ALTER SEQUENCE public.criteriakeys_id_seq OWNED BY public.criteriakeys.id;


--
-- Name: playgrounds; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.playgrounds (
    id integer NOT NULL,
    name character varying(255),
    mapid integer,
    agelevel character varying(255),
    totplay integer,
    opentopublic integer,
    invitation integer,
    howtogetthere integer,
    safelocation integer,
    shade integer,
    monitoring integer,
    programming integer,
    weather integer,
    seating integer,
    restrooms integer,
    drinkingw integer,
    activeplay integer,
    socialplay integer,
    creativeplay integer,
    naturualen integer,
    freeplay integer,
    specificcomments text,
    generalcomments text,
    compsum integer,
    modsum integer,
    graspvalue integer,
    playclass character varying(255),
    subarea text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    lat double precision,
    long double precision,
    google_placesid character varying(255),
    imageurl text,
    address character varying(255),
    is_school boolean,
    pleasant_surrounding_rating integer,
    fencing_rating integer,
    disabilities_rating integer,
    musical_features_rating integer,
    swings_rating integer,
    bucket_swings_rating integer,
    sprayground_play_fountain_rating integer,
    skate_park_rating integer,
    streams_rating integer,
    slide_ratings integer,
    climbing_structures_rating integer,
    spinning_structures_rating integer,
    rocking_structure integer,
    balance_features_rating integer,
    sandbox_rating integer,
    playhouse_rating integer,
    stage_rating integer,
    path_for_wheeled_toys_rating integer,
    basketball_courts_rating integer,
    surface_marked_for_games_rating integer,
    open_space_for_ball_play integer,
    woods_rating integer,
    garden_rating integer,
    natural_elements_rating integer,
    other_cool_features text,
    disabilities_comments text,
    zip_code character varying(255)
);


ALTER TABLE public.playgrounds OWNER TO alexandriaplays;

--
-- Name: playgrounds_id_seq; Type: SEQUENCE; Schema: public; Owner: alexandriaplays
--

CREATE SEQUENCE public.playgrounds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.playgrounds_id_seq OWNER TO alexandriaplays;

--
-- Name: playgrounds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alexandriaplays
--

ALTER SEQUENCE public.playgrounds_id_seq OWNED BY public.playgrounds.id;


--
-- Name: rails_admin_histories; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.rails_admin_histories (
    id integer NOT NULL,
    message text,
    username character varying(255),
    item integer,
    "table" character varying(255),
    month smallint,
    year bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.rails_admin_histories OWNER TO alexandriaplays;

--
-- Name: rails_admin_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: alexandriaplays
--

CREATE SEQUENCE public.rails_admin_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rails_admin_histories_id_seq OWNER TO alexandriaplays;

--
-- Name: rails_admin_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alexandriaplays
--

ALTER SEQUENCE public.rails_admin_histories_id_seq OWNED BY public.rails_admin_histories.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO alexandriaplays;

--
-- Name: users; Type: TABLE; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO alexandriaplays;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: alexandriaplays
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO alexandriaplays;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alexandriaplays
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alexandriaplays
--

ALTER TABLE ONLY public.aliases ALTER COLUMN id SET DEFAULT nextval('public.aliases_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alexandriaplays
--

ALTER TABLE ONLY public.criteriakeys ALTER COLUMN id SET DEFAULT nextval('public.criteriakeys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alexandriaplays
--

ALTER TABLE ONLY public.playgrounds ALTER COLUMN id SET DEFAULT nextval('public.playgrounds_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alexandriaplays
--

ALTER TABLE ONLY public.rails_admin_histories ALTER COLUMN id SET DEFAULT nextval('public.rails_admin_histories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alexandriaplays
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: aliases; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--

INSERT INTO public.aliases VALUES (2, 49, 'Mount Jefferson Park', NULL, NULL);
INSERT INTO public.aliases VALUES (3, 4, 'Eugene Simpson Stadium Park', NULL, NULL);
INSERT INTO public.aliases VALUES (4, 24, 'Hooff''s Run Park and Greenway', NULL, NULL);
INSERT INTO public.aliases VALUES (5, 67, 'Angel  Park', NULL, NULL);


--
-- Name: aliases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alexandriaplays
--

SELECT pg_catalog.setval('public.aliases_id_seq', 5, true);


--
-- Data for Name: criteriakeys; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--

INSERT INTO public.criteriakeys VALUES (1, 'totplay', 2, 'Yes', '');
INSERT INTO public.criteriakeys VALUES (2, 'totplay', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (6, 'howtogetthere', 3, 'Auto, public transit, pedestrians', NULL);
INSERT INTO public.criteriakeys VALUES (7, 'howtogetthere', 2, 'Auto and pedestrians', NULL);
INSERT INTO public.criteriakeys VALUES (8, 'howtogetthere', 1, 'Pedestrians and limited parking', NULL);
INSERT INTO public.criteriakeys VALUES (9, 'shade', 3, 'Plenty', NULL);
INSERT INTO public.criteriakeys VALUES (10, 'shade', 2, 'Some', NULL);
INSERT INTO public.criteriakeys VALUES (11, 'shade', 1, 'None', NULL);
INSERT INTO public.criteriakeys VALUES (15, 'restrooms', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (16, 'restrooms', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (17, 'drinkingw', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (18, 'drinkingw', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (19, 'activeplay', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (20, 'activeplay', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (21, 'socialplay', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (22, 'socialplay', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (23, 'creativeplay', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (24, 'creativeplay', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (25, 'naturualen', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (26, 'naturualen', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (27, 'freeplay', 2, 'Yes', NULL);
INSERT INTO public.criteriakeys VALUES (28, 'freeplay', 1, 'No', NULL);
INSERT INTO public.criteriakeys VALUES (5, 'opentopublic', 1, 'Residents only', '');
INSERT INTO public.criteriakeys VALUES (3, 'opentopublic', 4, 'Open to the public', '');
INSERT INTO public.criteriakeys VALUES (4, 'opentopublic', 3, 'Open to the public after school hours', '');
INSERT INTO public.criteriakeys VALUES (29, 'opentopublic', 2, 'Residents and Neighbors', '');
INSERT INTO public.criteriakeys VALUES (30, 'opentopublic', 5, 'Open to the public, fee required', '');
INSERT INTO public.criteriakeys VALUES (14, 'seating', 1, 'No', '');
INSERT INTO public.criteriakeys VALUES (12, 'seating', 3, 'Yes', '');
INSERT INTO public.criteriakeys VALUES (13, 'seating', 2, 'Limited', '');
INSERT INTO public.criteriakeys VALUES (31, 'drinkingw', 3, 'Yes in nearby nature or rec center', '');
INSERT INTO public.criteriakeys VALUES (32, 'restrooms', 3, 'Yes in nearby nature or rec center', '');
INSERT INTO public.criteriakeys VALUES (33, 'invitation', 1, 'Easy to find', '');
INSERT INTO public.criteriakeys VALUES (34, 'invitation', 2, 'Somewhat hidden', '');
INSERT INTO public.criteriakeys VALUES (35, 'invitation', 3, 'Can be hard to find', '');


--
-- Name: criteriakeys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alexandriaplays
--

SELECT pg_catalog.setval('public.criteriakeys_id_seq', 35, true);


--
-- Data for Name: playgrounds; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--

INSERT INTO public.playgrounds VALUES (6, 'Dora Kelley Park Ford Nature Center', 39, NULL, 1, 3, 3, 3, 3, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 2, 2, 'adj to rec ctr,mostly 5-12', ' - ', 11, 22, 242, 'SCHOOL', 'SubArea One', NULL, NULL, 38.8269049999999964, -77.1309140000000042, 'https://plus.google.com/101206238691208848946/about?gl=us&hl=en', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (23, 'South Port Apt', 127, '', 1, 1, 2, 2, 3, 1, 2, 1, 1, 3, 1, 1, 2, 1, 1, 1, 2, 'exposed,no shade for seating,no small grp play', '', 7, 16, 112, 'OTHER', 'No', NULL, '2016-03-16 00:05:32.630458', 38.8060449999999975, -77.1387610000000024, '', 'http://www.novaplays.org/pgimages/SouthPortApts.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (21, 'Ewald Park', 600, '2-5,5-12', 2, 3, 2, 2, 3, 1, 1, 1, 1, NULL, 1, 1, 2, 1, 1, 2, 2, 'looks like tot lot with equipment for older kids', 'This park contains play structures, seesaws and swings. This four-acre park includes a swimming pool, a lighted basketball court, a tennis court, a playground and an open area often used for pick-up soccer and football games.', 8, 19, 152, 'PARK', 'SubArea Two', NULL, '2016-03-16 00:06:22.764803', 38.8112670000000008, -77.1086310000000026, '', 'http://www.novaplays.org/pgimages/EwaldPark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (19, 'Cameron Parke Townes', 112, '', 2, 1, 1, 2, 3, 2, 2, 1, 2, 2, 1, 1, 2, 2, 2, 2, 1, 'no signage restricting public use', '', 9, 18, 162, 'Apartment-HOA', 'No', NULL, '2016-03-16 00:06:37.303465', 38.8053269999999983, -77.0947689999999994, '', 'http://www.novaplays.org/pgimages/CameronParkTownhomes.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (18, 'Maury Elementary School', 63, '5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 1, 1, 1, 2, 2, 'after hours only, lacks 2-5', 'The Maury school playgrounds have a garden and natural play area in addition to play equipment. It is next to Beach Park as well.', 8, 21, 168, 'SCHOOL', 'No', NULL, '2016-03-16 00:07:33.619339', 38.8127139999999997, -77.0644450000000063, '', 'http://www.novaplays.org/pgimages/MauryElemSchool.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (17, 'Samuel Tucker Elementary', 136, '2-5', 2, 3, 2, 2, 3, 3, 2, 1, 3, 3, 1, 1, 2, 2, 2, 1, 1, 'school,porticos,no social 2-5', 'School Playground', 8, 22, 176, 'SCHOOL', 'No', NULL, '2016-03-16 00:07:48.79015', 38.8054690000000022, -77.1275569999999959, '', 'http://www.novaplays.org/pgimages/SamuelTuckerElem.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (15, 'Exchange at Van Dorn', 117, '', 2, 1, 1, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 2, '', '', 10, 18, 180, 'Apartment-HOA', 'No', NULL, '2016-03-16 00:08:08.39422', 38.801673000000001, -77.1203440000000029, '', 'http://www.novaplays.org/pgimages/TheExchangeatVandoorn.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (9, 'Hillwood Apt', 120, '', 2, 3, 1, 2, 3, 1, 1, 1, 1, NULL, 1, 1, 2, 2, 2, 2, 2, ' - ', '2-12 structure?,overhead ladder', 12, 18, 216, 'Apartment-HOA', 'No', NULL, '2014-06-16 03:57:57.979404', 38.8313539999999975, -77.126813999999996, 'https://plus.google.com/114663951814921924479/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (10, 'Meadow Creek Apartments', 122, '2-5', 2, 3, 2, 2, 3, 1, 1, 1, 1, NULL, 1, 1, 2, 2, 2, 2, 2, 'phy domain-2-5 only,', '', 12, 18, 216, 'Apartment-HOA', 'No', NULL, '2014-06-16 04:19:56.658412', 38.8268010000000032, -77.1307269999999932, 'https://plus.google.com/108876908693298023554/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (12, 'Tarleton Park', 200, '', 2, 3, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 2, 2, 1, 2, 2, ' - ', 'Nature area with trails, open play area, playground, walking/biking trails and a sitting area.', 12, 17, 204, 'PARK', 'No', NULL, '2014-06-16 04:40:53.958701', 38.8087519999999984, -77.1083610000000022, 'https://plus.google.com/113952412151456176615/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (24, 'Hooffs Run Park Green', 73, '2-5,5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 2, 2, 2, 2, 2, 2, 'spin,spg needed', 'Playground scheduled for renovation summer 2014', 12, 24, 288, 'PARK', 'No', NULL, '2014-06-16 04:02:34.225068', 38.8095679999999987, -77.0594059999999956, 'https://plus.google.com/104077573533347968451/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (5, 'Charles Barrett Elementary School', 146, '2-5,5-12', 1, 3, 3, 3, 3, 3, 2, 3, 3, 1, 1, 1, 2, 1, 1, 2, 2, 'bus line,no 2-5', '', 9, 28, 252, 'SCHOOL', 'No', NULL, '2014-06-16 05:01:50.22845', 38.8416970000000035, -77.0761080000000049, 'https://plus.google.com/108593579071832020696/about?gl=us&hl=en', 'http://www.acps.k12.va.us/profiles/barrett.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (3, 'Chinquapin Rec Center', 901, '5-12', 2, 2, 1, 3, 3, 3, 3, 1, 3, 3, 3, 3, 2, 2, 2, 1, 2, '$4 charge for 30 min per child', 'Playground with baby swings, slides and structures to climb on. There are also large, beautiful fields, a basketball court and a picnic pavilion.', 11, 25, 275, 'INDOOR', 'SubArea Three', NULL, '2014-06-16 03:22:45.891348', 38.8217870000000005, -77.0815659999999951, 'http://alexandriava.gov/recreation/info/default.aspx?id=12352', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (4, 'Eugene Simpson Stadium Park', 40, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 3, 2, 2, 2, 2, 2, 2, 2, 'ymca adj,ballfield/ymca for RR,incl gardens', 'The park is next to the YMCA and has a playground with lots of space to play. There are baseball fields, basketball court, tennis couts and a dog park. The park also has a native plant and flower garden that children can visit.', 11, 23, 253, 'PARK', 'No', NULL, '2014-06-16 03:40:45.787097', 38.822420000000001, -77.0539909999999963, 'https://plus.google.com/104369426214440718388/about?gl=us&hl=en', 'http://gwslepthere.com/files/2010/06/x-toddler3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (13, 'James K Polk School', 42, '2-5', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 1, 1, 2, 2, 'school,not great 2-5,play other side bldg not 2-5', 'School Playground', 9, 22, 198, 'SCHOOL', 'No', NULL, '2014-06-16 04:06:07.880165', 38.8224480000000014, -77.1169929999999937, 'https://plus.google.com/106275800509912498015/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (28, 'John Adams School', 23, '2-5,5-12', 2, 1, 3, 3, 3, 1, 3, 2, 1, NULL, 1, 1, 2, 2, 2, 2, 2, 'shade on play,', 'The playground includes a natural play area with a small house and a wooden platform, or stage.', 15, 20, 300, 'SCHOOL', 'No', NULL, '2014-06-16 04:09:32.234804', 38.8341470000000015, -77.1246380000000045, 'https://plus.google.com/109828212239333359858/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (11, 'Patrick Henry School', 55, '', 2, 3, 3, 2, 3, 1, 2, 1, 1, NULL, 1, 1, 2, 2, 2, 1, 2, 'feels disconnected,no belted seats', 'School Playground', 11, 19, 209, 'SCHOOL', 'No', NULL, '2014-06-16 04:29:36.823539', 38.8176059999999978, -77.1110730000000046, 'https://plus.google.com/108482887043039779977/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (16, 'Powhatan Park', 51, '2-5', 2, 3, 3, 2, 2, 1, 1, 1, 1, 3, 1, 1, 2, 2, 2, 2, 2, 'not welcoming,fenced,next to busy st', 'Tot lot playground, tennis courts, basketball court and small grassy sections for general play.', 10, 18, 180, 'PARK', 'No', NULL, '2014-06-16 04:32:00.969999', 38.8177289999999999, -77.0489919999999984, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (2, 'Windmill Hill Park', 95, '2-5,5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 2, 1, NULL, 2, 2, 2, 2, 2, 'one bench,fountain far,nice sand,', 'The surfacing for the playground is woodchips, and there is an open grassy field space for sports (or kite flying!). There is a picnic like table that has a chess board, areas to feed ducks along the Potomac River and a volley ball court that can second for a sandbox although there is a sandbox on the playground. There is also a tube slide, parallel bars, voice caller, rope climb and many other things.', 13, 22, 286, 'PARK', 'No', NULL, '2014-06-16 04:43:56.399604', 38.7996679999999969, -77.0412429999999944, 'https://plus.google.com/111069396770764010960/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (56, 'Chatham Square HOA', 113, '', 2, 3, 2, 2, 3, 2, 2, 1, 2, 3, 1, 1, 1, 2, 2, 2, 2, '', '', 9, 21, 189, 'Apartment-HOA', 'No', NULL, '2016-03-16 00:04:15.355431', 38.8087810000000033, -77.0432299999999941, '', 'http://www.novaplays.org/pgimages/ChathamSquare.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (48, 'James Mulligan Park', 142, '5-12', 2, 3, 2, 2, 2, 2, 2, 1, 2, 3, 1, 1, 2, 2, 1, 2, 2, 'wooded,needs light/color,', 'Natural wooded area, picnic area and a playground.', 12, 18, 216, 'PARK', 'No', NULL, '2014-06-16 04:07:20.161011', 38.8384890000000027, -77.1073020000000042, 'https://plus.google.com/106827763900658106106/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (34, 'Chetworth Park', 49, '', 2, 3, 2, 1, 3, 2, 2, 1, 2, 3, 1, NULL, 2, 2, 2, 2, 2, ' - ', 'This park contains play structures and swings suitable for children ages 2-5.', 12, 22, 264, 'PARK', 'SubArea Four', NULL, '2014-06-16 03:19:25.304891', 38.8196370000000002, -77.0459529999999972, 'http://alexandriava.gov/recreation/info/default.aspx?id=12284#Chetworth', 'http://imagesus.homeaway.com/mda01/de1cd5c1-c971-4e4d-9456-4120e92dea80.1.10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (36, 'Stevenson Park', 71, '2-5,5-12', 2, 3, 3, 2, 3, 3, 2, 1, 3, 3, 1, 1, 2, 2, 2, 2, 2, 'nice shelter, shade trees,variety', 'Small community park with playground,surrounded by tall trees. Shaded picnic tables and basketball court and baseball field.', 11, 23, 253, 'PARK', 'No', NULL, '2014-06-16 02:19:47.639642', 38.811435000000003, -77.1432570000000055, 'https://plus.google.com/102483809053870884915/about?gl=us&hl=en', 'http://pp3.walk.sc/s500/production/67109.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (53, 'Armory Tot Lot', 93, '2-5', 2, 3, 3, 2, 3, 2, 1, 1, 2, 3, 1, 1, 2, 2, 1, 2, 2, 'indoor play area in mall, few plastic structures', 'Coffee shop is two short block away and has bathroom', 9, 22, 198, 'PARK', 'No', NULL, '2014-06-16 02:23:53.719802', 38.8030210000000011, -77.0436720000000008, 'http://alexandriava.gov/recreation/info/default.aspx?id=12284#armory', 'http://alexandriava.gov/uploadedImages/recreation/parks/IMG_1100.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (54, 'Charles Barrett Recreation Center', 145, '5-12', 2, 3, 3, 3, 3, 2, 2, 1, 2, 2, 3, 3, 2, 1, 1, 2, 2, 'bus line,surfacing has grass,one bench', '', 9, 22, 198, 'Recreation Center', 'No', NULL, '2014-06-16 03:13:19.673581', 38.8417310000000029, -77.076123999999993, 'https://plus.google.com/111933598085281792616/about?gl=us&hl=en', 'http://alexandriava.gov/uploadedImages/recreation/info/CharlesBarrettRecCenter.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (31, 'Douglas MacArthur School', 58, '', 2, 3, 3, 3, 3, 2, 1, 1, 2, NULL, 1, 1, 2, 2, 2, 2, 2, 'school, good equip,large open areas', 'School playground with slides, swings, lots of climbing contraptions and a nice open field.', 13, 22, 286, 'SCHOOL', 'No', NULL, '2014-06-16 03:36:56.241659', 38.816004999999997, -77.0831100000000049, 'https://plus.google.com/101305828538360429512/about?gl=us&hl=en', 'http://www.studio39.com/wp-content/uploads/2012/11/08_DouglasMacArthur.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (50, 'Fairlington United Methodist Church', 118, '', 1, 3, 2, 1, 3, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 2, 'looks private,hard to park,busy st,need 2-5', '', 11, 19, 209, 'Church', 'No', NULL, '2014-06-16 03:45:59.084478', 38.8330490000000026, -77.0961709999999982, 'https://plus.google.com/107417752515570172037/about?gl=us&hl=en', 'http://api.ning.com/files/VzgFwBdBjDWBJr7KVWFPPxpqZI3bcpLy3ezgTrFv-2Lz6n*XTTCSnDTgVlBeQpd8tMaT81y0Wc1pBAepexu7MwUwfGz679nw/Christianonslide.jpg?width=450&height=600', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (43, 'George Mason Elementary School', 36, '2-5', 2, 3, 2, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 2, 1, 2, 2, 'more seating,some 2-5', 'School Playground', 11, 20, 220, 'SCHOOL', 'No', NULL, '2014-06-16 03:53:23.776582', 38.828909000000003, -77.0724050000000034, 'https://plus.google.com/112390418381411924551/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (52, 'Goat Hill Park', 29, '5-12', 2, 3, 2, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 1, 'shade,more benches,more free play', 'The park has one piece of very nice play equipment and one set of toddler and special needs swings. The flooring was the compressed rubber with padding underneath with makes it easy to play on. The park get some shade from the surrounding trees. The playground is in the Warwick Village neighborhood.', 10, 20, 200, 'PARK', 'No', NULL, '2014-06-16 03:56:05.103203', 38.8323669999999979, -77.061009999999996, 'http://alexandriava.gov/recreation/info/default.aspx?id=12286#Goat_Hill', 'http://novaoutdoors.com/nova/wp-content/uploads/2012/05/DSC_0849-e1384868045904.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (46, 'Landover Park', 21, '2-5,5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 1, 1, 2, 2, 'seatwalls,no social/intell for 2-5', 'very new, limited for 2-5', 10, 22, 220, 'PARK', 'No', NULL, '2014-06-16 04:14:11.410592', 38.8352360000000019, -77.0617539999999934, 'http://alexandriava.gov/recreation/info/default.aspx?id=12294#Landover', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (40, 'Lyles Crouch School', 94, '5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 1, 2, 2, 2, 'assumes after school,more 2-5,gardens', 'School Playground', 11, 22, 242, 'SCHOOL', 'No', NULL, '2014-06-16 04:15:15.000922', 38.8001319999999978, -77.0466540000000037, 'https://plus.google.com/112539546640725521592/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (49, 'Mt Jefferson Park Green', 30, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 2, ' - ', 'Playground has rubber surfacing, a large spinning wheel for kids to ride on, and slides.', 10, 21, 210, 'Park', 'No', NULL, '2014-06-16 04:23:00.920883', 38.8306510000000031, -77.0558170000000047, 'https://plus.google.com/108345847185228583127/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (38, 'Mt Vernon Elementary School', 38, '5-12', 2, 3, 3, 3, 3, 2, 2, 1, 2, 3, 1, 1, 1, 1, 1, 2, 2, 'on bus route?,sparse mulch,adj rec ctr', 'There are two play areas here.  Cafes and restaurants nearby on Mt. Vernon Ave.', 10, 25, 250, 'SCHOOL', 'No', NULL, '2014-06-16 04:24:23.641574', 38.8277829999999966, -77.0592510000000033, 'https://plus.google.com/116318907109383488449/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (44, 'Nannie J Lee Recreation', 129, '2-5', 2, 3, 1, 2, 3, 2, 2, 1, 2, 3, 2, 2, 2, 2, 2, 2, 2, 'rec ctr use? more 2-5', '', 10, 22, 220, 'REC CENTER', 'No', NULL, '2014-06-16 04:25:25.928614', 38.7964640000000003, -77.0530640000000062, 'https://plus.google.com/104227345177388388500/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (45, 'Park Fairfax', 133, '', 2, 1, 1, 2, 2, 3, 1, 1, 3, 3, NULL, NULL, 2, 2, 2, 2, 2, 'no spinning or swings', '', 10, 22, 220, 'Apartments-HOA', 'No', NULL, '2014-06-16 04:28:31.711399', 38.8357969999999995, -77.0777319999999975, 'http://www.parkfairfax.info/community-info/recreational-resources/tot-lots-3/', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (42, 'Potomac Greens HOA', 124, '', 2, 3, 1, 2, 3, 1, 1, 1, 1, 3, 1, 1, 2, 2, 2, 2, 2, '1/5/12', '', 12, 19, 228, 'OTHER', 'No', NULL, '2014-06-16 04:31:07.497083', 38.8317309999999978, -77.0456389999999942, 'https://plus.google.com/107291447163352006328/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (35, 'Watergate at Landmark Condominium', 131, '', 2, 1, 2, 2, 3, 2, 3, 1, 2, NULL, NULL, NULL, 2, 2, 2, 2, 2, 'gated,lifeguard,needs shade,nice grassy hill', 'part of large pool complex in gated community', 12, 22, 264, 'OTHER', 'No', NULL, '2014-06-16 04:41:47.4693', 38.8086890000000011, -77.1416799999999938, 'https://plus.google.com/113879097084597048215/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (70, 'Landmark Terrace', 134, '', 2, 1, 2, 2, 2, 2, 2, 1, 2, NULL, 1, 1, 2, 2, 2, 2, 1, 'no ADA,close to pkg,needs fence,upgrade sufg', '', 9, 16, 144, 'Apartment-HOA', 'No', NULL, '2016-03-16 00:00:16.690512', 38.8091750000000033, -77.133931000000004, '', 'http://www.novaplays.org/pgimages/LandmarkTerrace.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (68, 'Woodbine Park', 700, '2-5,5-12', 2, 3, 3, 2, 3, 1, 1, 1, 1, 3, 1, 1, 2, 1, 1, 2, 2, '', '', 8, 20, 160, 'PARK', 'No', NULL, '2016-03-16 00:00:34.6559', 38.8292890000000028, -77.0824850000000055, '', 'http://www.novaplays.org/pgimages/WoodbinePark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (62, 'Chinquapin Park', 140, '', 2, 3, 2, 2, 2, 3, 1, 1, 3, 2, 1, 1, 2, 2, 1, 2, 2, 'worn,no play during school, shelter, seating far', 'more 2-5,nice but little use', 9, 19, 171, 'PARK', 'SubArea Three', NULL, '2016-03-16 00:02:31.153307', 38.8218260000000015, -77.0816449999999946, '', 'http://www.novaplays.org/pgimages/ChinquapinPark.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (61, 'Sunset Park', 81, '2-5', 2, 3, 3, 1, 2, 1, 1, 1, 1, 3, 1, 1, 2, 2, 2, 2, 2, 'on busy st,no pakg,', 'Small playground with swings and other play structures suitable for children ages 2-5. The equipment is modern, clean, and the park is equipped with benches, plenty of trees and a fenced in facility.', 10, 18, 180, 'PARK', 'No', NULL, '2016-03-16 00:02:57.963128', 38.8072179999999989, -77.0621649999999931, '', 'http://www.novaplays.org/pgimages/SunsetMiniPark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (60, 'Mason Avenue Park', 46, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 3, 1, 1, 2, 2, 1, 2, 1, 'spin spring,swing,all 2-5,sm free play, worn grass', 'The park contains a rubber surface and has play structures, a climbing wall, seesaws and swings.', 9, 20, 180, 'PARK', 'No', NULL, '2016-03-16 00:03:16.561633', 38.8208040000000025, -77.0622440000000068, '', 'http://www.novaplays.org/pgimages/MasonAvePark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (58, 'George Washington Middle School', 59, '', 2, 3, 3, 3, 3, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 1, 'metro nearby,no spin,open lawn is exposed', 'Playground designed for preschoolers. Fenced in and faces Mt. Vernon Ave.', 9, 21, 189, 'SCHOOL', 'No', NULL, '2016-03-16 00:03:55.672995', 38.815218999999999, -77.0563700000000011, '', 'http://www.novaplays.org/pgimages/GeorgeWashingtonMiddleSchool.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (71, 'ARHA_Henry', 105, '', 2, 3, 3, 2, 3, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 'open,exposed,no seating,more 2-5', '', 7, 18, 126, 'Apartment-HOA', 'No', NULL, '2014-06-16 02:10:27.284751', 38.816198, -77.1118230000000011, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (79, 'Montgomery Park', 62, '2-5,5-12', 2, 3, 3, 2, 3, 3, 2, 1, 3, 2, 1, NULL, 2, 2, 2, 1, 2, 'pavilion,needs inside seating,lacks spinning', 'The playground has three separate play structures: a dinosaur slide, castle slide and the main play structure with a double slide, twisty slide and climbing bars. There is also a double teeter totter and 4 swings (2 regular and 2 bucket). The surface is mulch.', 10, 24, 240, 'PARK', 'No', NULL, '2014-06-16 04:21:20.833641', 38.8144360000000006, -77.0411619999999999, 'https://plus.google.com/101907058020918098326/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (75, 'Saxony Square', 137, '', 1, 1, 2, 2, 2, 2, 2, 1, 2, 3, 1, 1, 1, 1, 1, 1, 1, 'close to pkg,nice bench,no phys 2-5', '', 5, 17, 85, 'OTHER', 'No', NULL, '2014-06-16 04:35:18.701158', 38.8222579999999979, -77.133624999999995, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (82, 'Brookville Townhomes', 111, '5-12', 1, 2, 1, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 'public housing?,feels private,close to pkg lot', 'not really for 2-5', 7, 16, 112, 'Apartment-HOA', 'No', NULL, '2014-06-15 05:13:09.018889', 38.8198519999999974, -77.1245959999999968, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (65, 'ARHA_Whiting', 107, '2-5', 2, 1, 2, 2, 3, 2, 2, 1, 2, 2, 1, 1, 2, 2, 2, 2, 1, 'more 2-5, no space for free play', '', 9, 18, 162, 'Apartment-HOA', 'No', NULL, '2014-06-16 02:13:35.437394', 38.8101690000000019, -77.136895999999993, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (66, 'Holmes Run Park-S. Jordan St.', 98, '5-12', 1, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 1, 1, 2, 1, 'sufacing needs renovated,across bridge RR/water', '', 7, 23, 161, 'PARK', 'No', NULL, '2014-06-16 03:59:28.022425', 38.8120529999999988, -77.1096170000000001, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (78, 'Hunter Miller Park', 76, '2-5,5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 2, 'some dead grass,no todd swg,', 'This playground has a slide and has both baby swings and swings suitable for elementary school children. It has a spinning wheel that kids seem to enjoy.', 11, 22, 242, 'PARK', 'No', NULL, '2014-06-16 04:05:07.232529', 38.8078610000000026, -77.0522480000000058, 'https://plus.google.com/114642175620826529860/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (84, 'Mayflower Square Condos', 141, '', 1, 2, 1, 2, 3, 2, 2, 1, 2, NULL, 1, 1, 1, 1, 1, 1, 2, 'no 2-5,swgs missing', '', 6, 17, 102, 'Apartment-HOA', 'No', NULL, '2014-06-16 04:18:57.386506', 38.8201109999999971, -77.1356360000000052, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (81, 'ARHA_Tancil Ct.', 103, '', 1, 2, 3, 2, 3, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 'needs better lands,lim shade,more 2-5', '', 6, 19, 114, 'Apartment-HOA', 'No', NULL, '2014-06-16 02:12:32.903358', 38.809626999999999, -77.0421410000000009, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (74, 'Bennington Crossing Apt', 108, '', 1, 1, 2, 2, 2, 1, 2, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 2, 'no fence bet play/park,needs landscaping/shade', '', 6, 15, 90, 'Apartment-HOA', 'No', NULL, '2014-06-16 02:37:40.436975', 38.8204300000000018, -77.1323110000000014, '', 'http://benningtoncrossings.com/wp-content/uploads/2013/05/bennington_playground.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (69, 'Buchanan Park', 201, '2-5', 1, 3, 2, 3, 3, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 2, ' - ', 'This playground is currently being re-built in conjunction with the Jefferson Houston School construction.  ', 8, 19, 152, 'PARK', 'No', NULL, '2014-06-16 03:00:28.045169', 38.8086070000000021, -77.0572030000000012, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (76, 'Four Mile Run Park', 1, '5-12', 2, 3, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 2, 'pleasant,remote,along trail, no benches,no 2-5 sw', 'The playground has one piece of play equipment and both types swings. Restrooms are porta potties by the soccer fields (closest to the play equipment) and also indoor restrooms by the baseball fields. There isn’t much shade over the equipment but there are a few trees around the equipment for the kids to get out of the sun. To access the play equipment it’s easier to park at the Mt. Vernon Avenue entrance and take the path to the park.', 9, 15, 135, 'PARK', 'No', NULL, '2014-06-16 03:50:28.643299', 38.8422669999999997, -77.0612529999999936, 'https://plus.google.com/111336346757609350007/about?gl=us&hl=en', 'http://novaoutdoors.com/nova/wp-content/uploads/2012/07/featured-image.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (77, 'Charles Houston Rec Center', 900, '5-12', 2, 2, 2, 3, 3, 3, 3, 1, 3, 3, 2, 2, 2, 2, 2, 1, 2, 'for kids 2-5, $4 charge for 45 min per child', 'Fenced in playground on rubber surfacing', 12, 26, 312, 'Recreation Center', 'No', NULL, '2014-06-16 03:14:50.929573', 38.812973999999997, -77.047820999999999, 'https://plus.google.com/118020711158193562510/about?gl=us&hl=en', 'http://alexandriava.gov/uploadedImages/recreation/info/CharlesHouston.JPG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (57, 'Landmark Mall', 902, '2-5', 2, 3, 3, 2, 2, 3, 2, 1, 3, 3, 2, 1, 2, 2, 1, 1, 2, 'in mall, not a lot of equip,primary user under 6', '', 8, 24, 192, 'INDOOR', 'Yes', NULL, '2014-06-16 04:10:40.9637', 38.8161339999999981, -77.1318619999999981, 'https://plus.google.com/107788900110772782777/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (64, 'Newport Village Apt', 132, '2-5,5-12', 2, 1, 1, 2, 3, 1, 2, 1, 1, 3, 1, 1, 2, 2, 2, 2, 2, '1 struc, 2-12 yrs afe , some for 2-5', '', 11, 15, 165, 'Apartment-HOA', 'No', NULL, '2014-06-16 04:26:47.938753', 38.8375010000000032, -77.1112959999999958, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (83, 'Old Town Village', 789, '', 1, 2, 1, 2, 3, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 2, 2, '', '', 7, 15, 105, 'Apartment-HOA', 'No', NULL, '2014-06-16 04:27:35.197643', 38.8035300000000021, -77.0540550000000053, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (55, 'Southern Towers', 128, '2-5,5-12', 1, 1, 2, 2, 3, 1, 2, 1, 1, 3, 1, 1, 2, 2, 1, 2, 2, 'res or guest only,5-12 struc,6 swings', 'There are two playgrounds at Southern Towers. One is for 2-5 year-olds and the other for 5-12 year-olds.', 11, 18, 198, 'OTHER', 'No', NULL, '2014-06-16 04:37:43.603608', 38.8326300000000018, -77.112797999999998, 'https://plus.google.com/117325321240407624505/about?gl=us&hl=en', 'http://pp.walk.sc/s225/p/39122/9a5771.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (30, 'Beverley Hills Church Preschool Playground', 109, '', 2, 3, 3, 2, 3, 2, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 'limited use,after school use,good social', 'This playground has boulders to climb on, a fort with a slide, a music garden and other fun natural features.', 13, 22, 286, 'School', 'No', NULL, '2014-06-16 02:48:33.173123', 38.8387039999999999, -77.0686230000000023, 'https://plus.google.com/106901648799695482578/about?gl=us&hl=en', 'http://d1byvvo791gp2e.cloudfront.net/public/assets/media/images/000/256/473/images/size_550x415_IMG_2921.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (88, '840 N. Alfred Street Park ', NULL, '2-5', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', 'Play area has activity panels and rubber mound.  Park has stage, open turf area.', NULL, NULL, NULL, 'Park', 'No', '2014-06-15 19:59:12.891469', '2014-06-15 19:59:12.891469', 38.8137609999999995, -77.0473910000000046, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (91, 'Potomac Greens Park', NULL, '5-12', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', 'Playground connects to wetland boardwalk/trail.  Park also has a small gazebo and large grass area.', NULL, NULL, NULL, 'Park', 'No', '2014-06-15 20:12:42.42343', '2014-06-15 20:12:42.42343', 38.8308940000000007, -77.0452420000000018, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (92, 'Woodmont Park Apartments', NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, 'Apartment-HOA', 'No', '2014-06-15 20:17:36.066476', '2014-06-15 20:17:36.066476', 38.8221929999999986, -77.1302570000000003, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (93, 'Tower 2000 Apartments', NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, 'Apartment-HOA', 'No', '2014-06-15 20:20:35.223496', '2014-06-15 20:21:35.81762', 38.8191720000000018, -77.1367719999999935, '', 'http://medialibrarycdn.propertysolutions.com/websites_media/tower2000apartments.com/cached_thumbs/640x480/5252f3cc58cf9565.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (94, 'The Seasons Condos', NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, 'Apartment-HOA', 'No', '2014-06-15 20:27:08.077961', '2014-06-15 20:27:08.077961', 38.8187939999999969, -77.1344760000000065, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (67, 'Angel Park', 101, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 2, 1, 1, 2, 1, 2, 2, 1, 'close to road,benches not shaded, more 2-5', 'Small fenced playground area which is appropriate for kids of all ages. There are no swings but there is climbing equipment and slides. There are also fields for little league and T-ball, soccer, a basketball court and picnic area with a grill.', 8, 20, 160, 'PARK', 'SubArea Three', NULL, '2016-03-16 00:01:05.956211', 38.8098930000000024, -77.0751510000000053, '', 'http://www.novaplays.org/pgimages/AngelPark.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (14, 'ARHA_Oronoco', 106, '', 1, 3, 2, 2, 3, 2, 2, 1, 2, 3, 1, 1, 1, 2, 2, 2, 2, 'pres public housing,no 2-5,grass area fenced', '', 9, 21, 189, 'Apartment-HOA', 'No', NULL, '2014-06-16 02:11:59.884142', 38.809193999999998, -77.0427679999999953, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (90, 'Jones Point Park', NULL, '2-5,5-12', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 2, NULL, NULL, NULL, NULL, NULL, '', 'Park is operated by the National Park Service.  Park has two playgrounds, walking trails, open grass fields, historic lighthouse, ballcourts, fishing areas', NULL, NULL, NULL, 'Park', 'No', '2014-06-15 20:06:53.576761', '2014-06-16 05:01:14.697968', 38.7932891999999967, -77.0420608999999956, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (95, 'Chinquapin Park Rec Center Soft Playroom', NULL, '2-5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, '', 'Has soft play materials, slides, bouncy balls, and ball pit.', NULL, NULL, NULL, 'Recreation Center', '', '2014-06-16 03:25:48.777657', '2014-06-16 03:25:48.777657', 38.8217870000000005, -77.0815659999999951, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (96, 'Chinquapin Park Recreation Center Pool', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, '', 'Has large pool with lap lanes and children''s area, including long entry ramp.', NULL, NULL, NULL, 'Recreation Center', 'Yes', '2014-06-16 03:27:58.843144', '2014-06-16 03:27:58.843144', 38.8218260000000015, -77.0816449999999946, '', 'http://alexandriava.gov/uploadedImages/recreation/services/Rixse(1).jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (87, 'Sentinel of Landmark', 126, '', 1, 2, 0, 1, 0, 1, 0, 0, 0, NULL, 1, 1, 1, 1, 1, 1, 1, '', '', 0, 0, 0, 'Apartment-HOA', 'No', NULL, '2014-06-16 04:36:01.5522', 38.8123480000000001, -77.1408619999999985, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (80, 'Cora Kelly School', 11, '5-12', 2, 3, 2, 1, 2, 1, 1, 1, 1, 3, 3, 3, 2, 1, 1, 1, 2, 'RR around bldg, not much 2-5, location is hidden', 'School playground', 8, 17, 136, 'School', 'No', NULL, '2016-03-15 23:59:55.528115', 38.8381889999999999, -77.0582789999999989, 'https://plus.google.com/110721491788864478790/about?gl=us&hl=en', 'http://www.novaplays.org/pgimages/CoraKellySchool.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (89, 'Potomac Yard Park - Swann Ave.', NULL, '2-5,5-12', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, '', 'Playgrounds are separated into 2-5 and 5-12 play areas.  2-5 play area features activity maze.  Each playground has a play structure than can accommodate a mobility device user.', NULL, NULL, NULL, 'Park', 'No', '2014-06-15 20:04:26.297413', '2014-06-16 05:00:18.301542', 38.8291477, -77.0479653999999954, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (41, 'East Del Ray Avenue Park', 800, '2-5', 2, 3, 3, 2, 3, 1, 2, 1, 1, 3, 1, 1, 2, 2, 2, 2, 2, 'naturally designed play eqip, stumps, rocks,topo', 'A log themed teeter totter, trio of tree stumps, rocks and a log to balance on, a giant log to climb on or crawl through, and a small slide that doubles as a rock climbing structure. There are two raised hill mounds to climb across, a chess board and two chalk boards built into the wall. ', 11, 21, 231, 'PARK', 'No', NULL, '2016-03-16 00:04:47.061287', 38.8256259999999997, -77.0615920000000045, 'http://bit.ly/1e3ozWv', 'http://www.novaplays.org/pgimages/edelraypark.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (39, 'Armistead Boothe Park', 87, '2-5,5-12', 2, 3, 3, 2, 2, 3, 2, 1, 3, 2, 2, 1, 2, 2, 2, 2, 2, 'feels open, scattered,shelter,more 2-5', 'Playground, picnic pavilion with grill, restrooms, athletic fields, basketball court, tennis courts and walking/biking trails. This park is near Samuel Tucker Elementary School', 11, 22, 242, 'PARK', 'No', NULL, '2014-06-16 05:00:56.886735', 38.8054839999999999, -77.1275779999999997, 'https://plus.google.com/112898481605845199202/about?gl=us&hl=en', 'https://cdn3.gbot.me/photos/Va/bk/1361720925/-Postcard_of_Armistead_L_B-20000000005638366-500x375.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (33, 'Beach Park', 67, '2-5,5-12', 2, 3, 3, 2, 3, 2, 1, 1, 2, 2, 1, 1, 2, 2, 2, 2, 2, 'need shade for seating,more benches,need baby swgs', 'This park features two play structures, spring riders, and swings suitable for preschool or elementary school aged  children. There is also an open lawn area.', 12, 23, 276, 'PARK', 'No', NULL, '2014-06-16 05:01:33.944927', 38.813364, -77.0658370000000019, 'https://plus.google.com/104871444086354547996/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (7, 'Ben Brenman Park', 99, '', 2, 3, 2, 2, 3, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 'feat not engaging,lacks int social,fence sep kids', 'The park has a playground, trails and paths, picnic areas with seating and grills, a pond, a fenced dog park and more.', 9, 25, 225, 'PARK', 'No', NULL, '2014-09-07 18:27:40.999074', 38.8081620000000029, -77.1132570000000044, 'https://plus.google.com/101871333318321898748/about?gl=us&hl=en', 'http://condo-alexandria.com/condo-blog/content/binary/ben-b.jpg', '4800 Brenman Park Dr Alexandria, VA 22304', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (85, 'Summers Grove Townhomes', 138, '', 2, 2, 3, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 'play are on sport court,makeshift playgrd', '', 7, 13, 91, 'Apartment-HOA', 'No', NULL, '2016-03-15 23:59:33.40338', 38.8011920000000003, -77.1326860000000067, '', 'http://www.novaplays.org/pgimages/SummersGroveTownhomes.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (22, 'ARHA_Braddock', 104, '', 1, 3, 3, 3, 2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 'serves housing,on bus line,location unappealing', '', 7, 17, 119, 'Apartment-HOA', 'No', NULL, '2016-03-16 00:05:56.809179', 38.8310439999999986, -77.1071299999999979, '', 'http://www.novaplays.org/pgimages/ARHABraddock.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (47, 'Fort Ward Park Hist Site', 32, '2-5,5-12', 2, 3, 1, 2, 2, 2, 1, 1, 2, 3, 2, 2, 2, 2, 2, 2, 2, 'not ada, play isolated,2 sep structures', 'The park has a playground, picnic areas, an amphitheater and a dog park. There is also an on-site museum at Fort Ward explaining the history of the area.', 12, 18, 216, 'PARK', 'No', NULL, '2014-06-16 03:48:37.310563', 38.8311840000000004, -77.1005710000000022, 'https://plus.google.com/105490390078345261808/about?gl=us&hl=en', 'http://www.nova.joytroupe.com/wp-content/uploads/2011/04/IMG_9089.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (32, 'Jefferson Houston School', 77, '5-12', 2, 3, 3, 2, 3, 2, 2, 1, 2, NULL, 1, 1, 2, 2, 2, 2, 2, 'school use only during day,lg playgrd', 'This play area is currently under construction while the new school building is being completed', 14, 20, 280, 'SCHOOL', 'No', NULL, '2014-06-16 04:08:28.47087', 38.8080489999999969, -77.0547650000000033, 'https://plus.google.com/113655416565583281561/about?gl=us&hl=en', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (29, 'Beverly Park', 16, '2-5,5-12', 2, 3, 3, 2, 3, 3, 2, 1, 3, 2, 1, 1, 2, 2, 2, 2, 2, 'shelter,no spin,free toys', 'This park, also called "The Pit," consists of several play structures, seesaws and swings. There is also a hard court play area, picnic tables, sitting areas and wooded natural area', 13, 23, 299, 'PARK', 'No', NULL, '2014-06-16 04:59:59.128613', 38.8378439999999969, -77.0722650000000016, 'https://plus.google.com/103584699873785625173/about?gl=us&hl=en', 'https://www.our-kids.com/sites/default/files/styles/detail_page/public/images/beverlypark1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (37, 'Brookvalley Park-Ripley St. (Holmes Run)', 50, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, NULL, 1, 1, 2, 2, 2, 2, 2, 'kids stray onto path,good 2-5,2 struct. not 2-5', 'There is a playground, picnic areas, natural areas and walking/bicycle trails. Brookvalley is part of Holmes Run Park, home to the Bicentennial Tree, the oldest tree in Alexandria, as well as a natural habitat to many species of animals.', 12, 21, 252, 'Park', 'No', NULL, '2014-06-16 05:02:08.102819', 38.8188030000000026, -77.1262519999999938, 'https://plus.google.com/113385420946763251335/about?gl=us&hl=en', 'http://www.nova.joytroupe.com/wp-content/uploads/2012/04/100_3348-150x150.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (97, 'Holmes Run Park-Trail', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', 'There are picnic tables and grills, playgrounds and exercise stations for older children and adults within the park.', NULL, NULL, NULL, 'Park', 'No', '2014-06-16 04:01:25.817879', '2015-05-03 13:30:26.647989', 38.8120529999999988, -77.1096170000000001, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.playgrounds VALUES (63, 'Lynhaven Park', 20, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 3, NULL, NULL, 2, 2, 2, 1, 1, 'cluttered surroundings,people cut thru,rec ctr but', 'There are two main structures here all set in a blue hue. The first structure consist of four sets of modern climbers in addition to a chain cargo net and rock mountain. There are two metal cargo chain nets, two tic tac toe boards (a sports theme and state theme), tunnel, open window front, steering wheels, gear gadget, and speaker phone. There are two bucket and two regular swings and a spring rider that seats four.', 8, 21, 168, 'PARK', 'No', NULL, '2016-03-16 00:02:16.116262', 38.836545000000001, -77.056601999999998, 'http://alexandriava.gov/recreation/info/default.aspx?id=12294#Lynhaven', 'http://www.novaplays.org/pgimages/LynhavenPark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (59, 'Hume Springs Park', 19, '2-5,5-12', 2, 3, 3, 2, 2, 2, 2, 1, 2, 3, 1, 1, 2, 2, 2, 2, 2, 'auto across street, industrial feel, one bench', ' This park contains play sets, seesaws and bucket swings for toddlers. There is a newer structure with a tic tac toe game and an animal board game, three slides (a small, straight one, double slide and spiral version). There is a set of monkey bars and a neat set of plastic climbing discs. The playground is fenced in and has two entrances with a mulch surface.', 10, 18, 180, 'PARK', 'No', NULL, '2016-03-16 00:03:35.028995', 38.8368150000000014, -77.0591730000000013, '', 'http://www.novaplays.org/pgimages/HumeSpringsPark.JPG', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (25, 'EOS 21 Condo', 130, '', 2, 1, 1, 2, 3, 1, 2, 1, 1, 1, NULL, NULL, 2, 2, 2, 1, 2, 'needs trees,no2-5,grass nearby', '', 6, 15, 90, 'OTHER', 'SubArea Two', NULL, '2016-03-16 00:05:09.366843', 38.8127429999999976, -77.1263179999999977, '', 'http://www.novaplays.org/pgimages/EOS21.jpg', '', false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '');
INSERT INTO public.playgrounds VALUES (269, 'Key Elementary School', NULL, '0 to 12', 2, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2300 Key Blvd<br>Close to the Courthouse Metro Station.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:49.28605', '2016-04-14 09:26:49.487587', 38.89239259, -77.0868715400000042, '', 'http://www.novaplays.org/pgimages/Key.jpg', '2300 Key Blvd', true, 2, 3, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 2, '', '', '22201');
INSERT INTO public.playgrounds VALUES (268, 'Jennie Dean Park', NULL, '5 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '3630 27th St S<br>Monkey bars and wide bars; 2 swings, slides and stairs. Tennis courts. Path might not accommodate wheelchair.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:48.240672', '2016-04-14 09:27:15.30439', 38.8435259299999984, -77.0882485799999984, '', 'http://www.novaplays.org/pgimages/JennieDean.jpg', '3630 27th St S', false, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, '', '', '22206');
INSERT INTO public.playgrounds VALUES (225, 'Arlington Traditional School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '855 N Edison Street<br>Three mulched play areas, divided by age group. No shade, access to water while playing. Ample parking in school parking lot.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:34.820765', '2016-03-15 23:53:16.163947', 38.8799289600000009, -77.1220630899999975, '', 'http://www.novaplays.org/pgimages/ATS.jpg', '855 N Edison Street', true, 3, 3, 1, 1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, '', '', '22205');
INSERT INTO public.playgrounds VALUES (224, 'Arlington Science Focus School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 2, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1513 N Lincoln St<br>Across the street from Hayes Park.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:34.48691', '2016-03-15 23:53:43.067443', 38.8900865399999986, -77.1027577199999996, '', 'http://www.novaplays.org/pgimages/ScienceFocus.jpg', '1513 N Lincoln St', true, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 2, 2, 1, 2, 1, '', '', '22201');
INSERT INTO public.playgrounds VALUES (223, 'Arlington Mill Community & Senior Center', NULL, '2 to 5', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '909 S Dinwiddie St<br>Easy access to public transit. Parking garage access off of Dinwiddie Street. The playground is not very visible from the street; take steps to it from street level, or access it from ground level of the community center. Restrooms are inside the community center.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:34.310948', '2016-03-15 23:54:01.551746', 38.8564027400000001, -77.1123007999999999, '', 'http://www.novaplays.org/pgimages/ArlingtonMill.jpg', '909 S Dinwiddie St', false, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, '', '', '22204');
INSERT INTO public.playgrounds VALUES (222, 'Arlington Hall West Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '290 S Taylor St<br>
Handicap swing; very large open space. Youth soccer field.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:34.125989', '2016-03-15 23:54:38.135651', 38.8655681999999985, -77.1069252900000066, '', 'http://www.novaplays.org/pgimages/ArlingtonHallWest.jpg', '290 S Taylor St', false, 3, 1, 2, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, NULL, '', '', '22204');
INSERT INTO public.playgrounds VALUES (221, 'Alcova Heights Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '901 S George Mason Dr<br>
Tire swing. Play area not enclosed by fence. Field for baseball or kickball.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:33.95574', '2016-03-15 23:55:01.714435', 38.8611958299999998, -77.1024143599999974, '', 'http://www.novaplays.org/pgimages/AlcovaHeights.jpg', '901 S George Mason Dr', false, 3, 1, 1, 1, 2, 2, 1, 1, 2, 2, 2, 1, 1, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (220, 'Abingdon Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '3035 S Abingdon Street<br>

There is a track like structure in the open field next to park with pull up bars. It appears the buses come within walking distance of the school', NULL, NULL, NULL, '', '', '2015-05-10 22:06:33.163597', '2016-03-15 23:55:29.601419', 38.8398295000000005, -77.0960894499999938, '', 'http://www.novaplays.org/pgimages/Abingdon.jpg', '3035 S Abingdon Street', true, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 2, 3, '', '', '22206');
INSERT INTO public.playgrounds VALUES (229, 'Barcroft Elementary School', NULL, '2 to 12', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '625 S Wakefield Road', NULL, NULL, NULL, '', '', '2015-05-10 22:06:35.657854', '2016-03-15 23:51:58.506947', 38.8616192400000031, -77.1077921400000008, '', 'http://www.novaplays.org/pgimages/BarcroftElem.jpg', '625 S Wakefield Road', true, 2, 1, 1, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 2, 2, 1, 1, 2, 2, 1, 2, 2, 2, 2, '', '', '22204');
INSERT INTO public.playgrounds VALUES (236, 'Butler Holmes Park', NULL, '5 to 12', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '101 S Barton Street<br>Wheelchair accessible to park structures, no specific handicap structures. Picnic table is wheelchair-accessible.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:37.394335', '2016-03-15 23:49:11.40447', 38.8727144499999966, -77.085110599999993, '', 'http://www.novaplays.org/pgimages/ButlerHolmes.jpg', '101 S Barton Street', false, 3, 2, 2, 1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (235, 'Bon Air Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 2, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '850 N Lexington St<br>The playground is very hard to reach from 8th road north; access from the intersection of 5th road N and N Montague St. Has a boat ship theme; some of the play area has shade. There is a path for a nature walk that leads to two other playgrounds which are well shaded. Restrooms closed during part of the year. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:37.203731', '2016-03-15 23:49:29.010779', 38.8755918700000009, -77.133115630000006, '', 'http://www.novaplays.org/pgimages/BonAir.jpg', '850 N Lexington St', false, 3, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 2, 2, 3, '', '', '22205');
INSERT INTO public.playgrounds VALUES (234, 'Bluemont Park', NULL, '0 to 12', 2, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '601 N Manchester St<br>Small climbing web feature. Playground not visible from parking lot; follow the trail at the end of the lot, playground on the right.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:36.954461', '2016-03-15 23:50:02.704789', 38.8735407899999998, -77.1339269599999966, '', 'http://www.novaplays.org/pgimages/Bluemont.jpg', '601 N Manchester St', false, 3, 2, 2, 2, 2, 2, 1, 1, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 1, 2, 1, 2, '', '', '22203');
INSERT INTO public.playgrounds VALUES (242, 'Chestnut Hills Park', NULL, '0 to 12', 2, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, '', '2807 N Harrison St<br>The ART Bus 52 stops at corner of Harrison and Little Falls; short walk from there.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:38.569945', '2016-03-15 23:46:54.529047', 38.9006258000000003, -77.1426730099999958, '', 'http://www.novaplays.org/pgimages/ChestnutHills.jpg', '2807 N Harrison St', false, 3, 3, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2, 2, 1, 2, 1, 1, 1, 2, 1, 2, '', '', '22207');
INSERT INTO public.playgrounds VALUES (239, 'Carlin Springs Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '5995 S Carlin Springs Road', NULL, NULL, NULL, '', '', '2015-05-10 22:06:37.940407', '2016-03-15 23:48:14.155545', 38.8627055999999982, -77.1318924100000061, '', 'http://www.novaplays.org/pgimages/CarlinSprings.jpg', '5995 S Carlin Springs Road', true, 2, 3, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, '', '', '22204');
INSERT INTO public.playgrounds VALUES (233, 'Big Walnut Park', NULL, '5 to 12', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1915 N Harrison St', NULL, NULL, NULL, '', '', '2015-05-10 22:06:36.50191', '2016-03-15 23:50:22.564536', 38.8917563899999976, -77.1321642300000008, '', 'http://www.novaplays.org/pgimages/BigWalnut.jpg', '1915 N Harrison St', false, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (238, 'Carlin Hall Community Center & Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '5711 4th St S<br>Not all benches have back rests. Not well marked as a public playground.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:37.735425', '2016-03-16 00:09:25.501557', 38.8631952300000023, -77.1261620500000049, '', 'http://www.novaplays.org/pgimages/CarlinHall.jpg', '5711 4th St S', false, 3, 3, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, '', '', '22204');
INSERT INTO public.playgrounds VALUES (237, 'Campbell Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '737 S Carlin Springs Road<br>Large school playground area with interesting climbing structures and natural features such as a wetlands exhibit area.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:37.568609', '2016-03-15 23:48:35.630847', 38.8579008199999976, -77.12611493, '', 'http://www.novaplays.org/pgimages/Campbell.jpg', '737 S Carlin Springs Road', true, 2, 2, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (241, 'Cherrydale Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 2, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2176 N Pollard St<br>Very small neighborhood park, mulch surfacing. Not easily visible from the street. Parking available across the street. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:38.363453', '2016-03-15 23:47:11.79281', 38.8989086799999981, -77.1071064399999955, '', 'http://www.novaplays.org/pgimages/Cherrydale.jpg', '2176 N Pollard St', false, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', '', '22207');
INSERT INTO public.playgrounds VALUES (243, 'Claremont Elementary School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 1, NULL, NULL, NULL, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '4700 S Chesterfield Road', NULL, NULL, NULL, '', '', '2015-05-10 22:06:38.821987', '2016-03-15 23:46:29.785836', 38.8477287999999987, -77.1058818799999983, '', 'http://www.novaplays.org/pgimages/Claremont.jpg', '4700 S Chesterfield Road', true, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 2, 2, 2, 3, '', '', '22206');
INSERT INTO public.playgrounds VALUES (226, 'Ashlawn Elementary School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '5950 8th RD N<br>Plenty of parking.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:35.072929', '2016-03-15 23:52:58.171198', 38.8732219500000014, -77.1360777600000063, '', 'http://www.novaplays.org/pgimages/Ashlawn.jpg', '5950 8th RD N', true, 2, 3, 1, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 2, '', '', '22203');
INSERT INTO public.playgrounds VALUES (244, 'Clarenford Station Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1300 N Vermont St<br>Two play structures: one for smaller children and one for larger. The path for wheeled toys is a long stretch of sidewalk along a quiet street. Open space for ball play outside the gated play area. About four blocks from Ballston Metro Station. Unmetered street parking available for about 12 cars (2 hour).', NULL, NULL, NULL, '', '', '2015-05-10 22:06:39.059425', '2016-03-15 23:45:57.963129', 38.8873272499999985, -77.1154490199999998, '', 'http://www.novaplays.org/pgimages/ClarenfordStation.jpg', '1300 N Vermont St', false, 2, 3, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, '', 'Mulch surface.', '22201');
INSERT INTO public.playgrounds VALUES (228, 'Bailey''s Branch Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '990 S Columbus St<br>Street parking limited. Most of the play areas have a good amount of seating. Trees line one side of the park. Fenced-in swing area doesn''t have any benches.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:35.456502', '2016-03-15 23:52:17.587168', 38.8541934599999976, -77.1118644000000018, '', 'http://www.novaplays.org/pgimages/BaileysBranch.jpg', '990 S Columbus St', false, 3, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, '', '', '22204');
INSERT INTO public.playgrounds VALUES (245, 'Dawson Terrace', NULL, '0-12', 2, 3, NULL, 1, 3, 3, NULL, NULL, NULL, 3, 2, 2, NULL, NULL, NULL, NULL, NULL, '', '2133 N Taft St<br>Restrooms are located in the Community Center, which is only open for scheduled classes.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:39.24514', '2016-03-15 23:45:35.51723', 38.8994278300000005, -77.08287215, '', 'http://www.novaplays.org/pgimages/DawsonTerrace.jpg', '2133 N Taft St', false, 3, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 0, NULL, 1, 2, 1, 2, 2, 1, 2, '', '', '22201');
INSERT INTO public.playgrounds VALUES (246, 'Doctor''s Branch Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '1301 S George Mason Dr<br>Volleyball court. Street parking; Capital Bikeshare station by park. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:39.427536', '2016-03-15 23:45:18.460491', 38.8561208500000035, -77.0999547299999932, '', 'http://www.novaplays.org/pgimages/DoctorsBranch.jpg', '1301 S George Mason Dr', false, 3, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2, 2, 1, 2, 1, 1, 1, 2, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (252, 'Fillmore Park/ Long Branch Elementary', NULL, '2 to 12', 1, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '33 N Fillmore St', NULL, NULL, NULL, '', '', '2015-05-10 22:06:41.405797', '2016-04-14 09:35:22.723081', 38.874962680000003, -77.0891553499999986, '', 'http://www.novaplays.org/pgimages/Fillmore.jpg', '33 N Fillmore St', true, 3, 3, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, '', '', '22201');
INSERT INTO public.playgrounds VALUES (259, 'Glebe Road Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '4211 N Old Glebe Rd<br>Swings are the only play equipment.  There are also tennis courts, basketball courts, and an open asphalt court for ball play, scooters, chalk, etc.  Paths/trails through wooded areas, too. There is a parking lot. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:43.863959', '2016-04-14 09:32:01.25924', 38.9214095299999983, -77.1257554999999968, '', 'http://www.novaplays.org/pgimages/GlebePark.jpg', '4211 N Old Glebe Rd', false, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (250, 'Edison Park', NULL, '2 to 5', 1, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '213 N Edison St<br>Street parking only.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:40.583275', '2016-04-14 09:36:12.086192', 38.8693158700000012, -77.1210351600000052, '', 'http://www.novaplays.org/pgimages/Edison.jpg', '213 N Edison St', false, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, '', '', '22203');
INSERT INTO public.playgrounds VALUES (266, 'High View Park', NULL, '2 to 12', 1, 3, NULL, 3, 2, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1945 N Dinwiddie St<br>The 2-5 area is fenced in; the rest of the play area is partially fenced. There is a picnic pavilion.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:47.243696', '2016-04-14 09:28:12.758902', 38.8937345200000024, -77.1271382599999953, '', 'http://www.novaplays.org/pgimages/HighView.jpg', '1945 N Dinwiddie St', false, 3, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2, '', '', '22207');
INSERT INTO public.playgrounds VALUES (251, 'Fairlington Community Center & Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '3308 S Stafford St', NULL, NULL, NULL, '', '', '2015-05-10 22:06:40.909406', '2016-04-14 09:35:45.574159', 38.8345022199999974, -77.0871734400000008, '', 'http://www.novaplays.org/pgimages/Fairlington.jpg', '3308 S Stafford St', false, 3, 2, 2, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 2, 2, 2, 2, '', '', '22206');
INSERT INTO public.playgrounds VALUES (267, 'Jamestown Elementary', NULL, '2 to 5', 1, 2, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '3700 N Delaware Street<br>This a a playground with mostly climbing structures and large green space. It has the additional Gaga ball pit for a different type of ball play. Inaccessible by wheelchair; children with walkers or crutches could access some features.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:47.713822', '2016-04-14 09:27:44.045284', 38.9172156099999995, -77.1394408000000027, '', 'http://www.novaplays.org/pgimages/Jamestown.jpg', '3700 N Delaware Street', true, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (265, 'Henry Wright Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '4350 4th St N<br>Playground is accessible by wheelchair but no specific features for children with disabilities. Floor of the play area is rubber. Slides are metal. Gazebo and grassy area with sidewalk paths outside the fenced area.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:46.653324', '2016-04-14 09:28:42.984203', 38.8716386899999975, -77.1102702799999946, '', 'http://www.novaplays.org/pgimages/HenryWright.jpg', '4350 4th St N', false, 3, 3, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, '', '', '22203');
INSERT INTO public.playgrounds VALUES (257, 'Foxcroft Heights Park', NULL, '0 to 5', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '801 S Oak St<br>Great view of Arlington National Cemetery and Air Force monument. Playground is on an historical landmark. Dogs must be on leash. Limited parking; street parking in restricted zone areas.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:42.859165', '2016-04-14 09:32:59.037301', 38.8686873899999981, -77.0707962799999962, '', 'http://www.novaplays.org/pgimages/Foxcroft.jpg', '801 S Oak St', false, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, '', '', '22211');
INSERT INTO public.playgrounds VALUES (256, 'Fort Scott Playground', NULL, '0 to 12', 2, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '2800 S Fort Scott Dr<br>Play surface is mostly mulch. Tennis courts, baseball field available. Parking lot available; nearest bus route ~3 blocks away, down a relatively steep hill. Restrooms closed October to March.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:42.330701', '2016-04-14 09:33:26.054011', 38.8473700899999983, -77.0586063399999972, '', 'http://www.novaplays.org/pgimages/FortScott.jpg', '2800 S Fort Scott Dr', false, 3, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22202');
INSERT INTO public.playgrounds VALUES (260, 'Glencarlyn Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '301 S Harrison St<br>Not visible from any streets; need to know the location of the park, by car or walking. No rails on the bridge that goes over the stream. Walking trails around area. Shelters can be reserved. Outdoor grills.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:44.626136', '2016-04-14 09:31:15.010652', 38.8639784800000001, -77.1190970499999935, '', 'http://www.novaplays.org/pgimages/Glencarlyn.jpg', '301 S Harrison St', false, 3, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 1, 1, 1, 2, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (261, 'Gunston Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 2, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '2700 Lang St S<br>Restrooms and drinking water are located in the Community Center. Hours of operation are Monday-Friday 2 pm-9 pm; Saturday 9 am-4:30 pm; Sunday closed. 
', NULL, NULL, NULL, '', '', '2015-05-10 22:06:45.105934', '2016-04-14 09:30:48.761902', 38.8471791500000023, -77.0710137699999933, '', 'http://www.novaplays.org/pgimages/Gunston.jpg', '2700 Lang St S', false, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 1, 2, '', '', '22206');
INSERT INTO public.playgrounds VALUES (258, 'Glebe Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1770 N Glebe Road, Arlington VA 22207', NULL, NULL, NULL, '', '', '2015-05-10 22:06:43.383168', '2016-03-15 23:41:10.404177', 38.8925719400000034, -77.1210674099999949, '', 'http://www.novaplays.org/pgimages/Glebe.jpg', '1770 N Glebe Road', true, 3, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, '', '', '22207');
INSERT INTO public.playgrounds VALUES (262, 'Hayes park', NULL, '0 to 12', 2, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1516 N Lincoln St<br>Picnic area. Lighted basketball and tennis courts for nighttime use. Sprayground available Memorial Day to Labor Day. Wheelchair access.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:45.486382', '2016-04-14 09:30:24.137085', 38.8897077399999986, -77.1041912599999932, '', 'http://www.novaplays.org/pgimages/Hayes.jpg', '1516 N Lincoln St', false, 2, 3, 1, 1, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 1, 1, 1, 3, '', '', '22201');
INSERT INTO public.playgrounds VALUES (264, 'Henry Clay Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '3011 7th St N<br>Only the basketball court is accessible for children with disabilities. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:46.164713', '2016-04-14 09:29:27.919363', 38.8813043700000023, -77.0940474100000017, '', 'http://www.novaplays.org/pgimages/HenryClay.jpg', '3011 7th St N', false, 3, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2, '', '', '22201');
INSERT INTO public.playgrounds VALUES (263, 'HB Woodlawn School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 2, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '4100 Vacation Ln', NULL, NULL, NULL, '', '', '2015-05-10 22:06:45.773841', '2016-04-14 09:29:53.475965', 38.9004930799999968, -77.1112098300000071, '', 'http://www.novaplays.org/pgimages/HBWoodlawn.jpg', '4100 Vacation Ln', true, 2, 1, 1, -1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 2, 2, 2, NULL, '', '', '22207');
INSERT INTO public.playgrounds VALUES (273, 'Lubber Run Community Center', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '300 N Park Dr<br>Not visible from N. George Mason Drive; easiest access is through Park Drive. Public transportation around the area is limited. Restrooms available in the community center when open (Monday 9 am-6:30 pm; Tuesday 9 am-8:30 pm; Wednesday 9 am-9 pm; Thursday 9 am-6:30 pm; Friday 9 am-10 pm; closed Saturday and Sunday.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:50.219968', '2016-04-14 09:24:47.183306', 38.8733941200000004, -77.1139844700000054, '', 'http://www.novaplays.org/pgimages/LubberRun.jpg', '300 N Park Dr', false, 3, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 2, 1, 1, 3, '', '', '22203');
INSERT INTO public.playgrounds VALUES (284, 'Nina Park', NULL, '0 to 5', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '800 24th St S, Arlington VA 22202. Lots of riding toys and plastic play equipment for young children. Also features a "wave wall" and pretend boat. Surface includes grass and sand, but there are some areas accessible to children with physical disabilities. Bus stop ~1 block away. Easy to drive to, but only street parking only available with permit.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:53.880719', '2016-03-15 23:26:36.894046', 38.8520785099999983, -77.0597736600000047, '', 'http://www.novaplays.org/pgimages/Nina.jpg', '800 24th St S', false, 2, 3, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, '', '', '22202');
INSERT INTO public.playgrounds VALUES (272, 'Lee Community Center', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '5722 Lee Highway<br>Baseball field. Restrooms and drinking water are located in the Community Center. Hours of operation are Monday-Friday 9:30 am-6 pm; Saturday 9:30 am-5 pm; Sunday closed.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:50.001608', '2016-04-14 09:25:14.968313', 38.8946664500000026, -77.143193519999997, '', 'http://www.novaplays.org/pgimages/Lee.jpg', '5722 Lee Highway', false, 2, 3, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1, 2, 2, 1, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (281, 'Mosaic Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '538 N. Pollard St', NULL, NULL, NULL, '', '', '2015-05-10 22:06:52.975452', '2016-04-14 09:20:33.963785', 38.8779762699999978, -77.1069269500000019, '', 'http://www.novaplays.org/pgimages/Mosaic.jpg', '538 N. Pollard St', false, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, '', '', '22203');
INSERT INTO public.playgrounds VALUES (279, 'McKinley Elementary', NULL, '0 to 12', 2, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1030 N McKinley Road<br>Playground has mulch in play areas, which are separated by age group. Preschool area is over by the huts. There is no seating; trees and houses line the backside of the park. Located at a neighborhood schoo; may not be accessible during non school hours. The field  is for use by permit only. No dogs are allowed in the area. Ample parking in the school parking lot. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:52.432216', '2016-04-14 09:21:53.957726', 38.8786553500000025, -77.1467417199999943, '', 'http://www.novaplays.org/pgimages/McKinley.jpg', '1030 N McKinley Road', true, 3, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 1, 2, 1, '', '', '22205');
INSERT INTO public.playgrounds VALUES (276, 'Madison Manor Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '6225 12th Rd N<br>Tennis courts (reservations needed), soccer field, and covered pavilion with picnic tables and grill. Small parking lot, as well as street parking. Situated within a neighborhood, so families can also walk. Restrooms closed during winter months.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:51.711939', '2016-04-14 09:23:30.366698', 38.8820746800000023, -77.1502322700000036, '', 'http://www.novaplays.org/pgimages/MadisonManor.jpg', '6225 12th Rd N', false, 2, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 1, 1, '', '', '22205');
INSERT INTO public.playgrounds VALUES (271, 'Langston Brown Community Center &  Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 2, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '2121 N Culpeper St<br>Restrooms and drinking water are located in the Community Center. Hours of operation are Monday-Friday 9 am-10 pm; Saturday 9 am-4 pm; Sunday closed.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:49.735625', '2016-04-14 09:25:43.242713', 38.895774320000001, -77.1265597600000063, '', 'http://www.novaplays.org/pgimages/LangstonBrown.jpg', '2121 N Culpeper St', false, 2, 3, 2, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 2, '', '', '22207');
INSERT INTO public.playgrounds VALUES (278, 'Maywood Park', NULL, '0 to 5', 2, 3, NULL, 1, 3, 2, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '3210 22nd St N<br>Small playground for young children.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:52.201072', '2016-04-14 09:22:30.613184', 38.8975324499999999, -77.1001594600000004, '', 'http://www.novaplays.org/pgimages/Maywood.jpg', '3210 22nd St N', false, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, '', '', '22201');
INSERT INTO public.playgrounds VALUES (286, 'Oakgrove Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 2, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1606 N Quincy St, Arlington VA 22207', NULL, NULL, NULL, '', '', '2015-05-10 22:06:54.332255', '2016-03-15 23:25:52.107941', 38.8905134500000003, -77.1088667300000026, '', 'http://www.novaplays.org/pgimages/Oakgrove.jpg', '1606 N Quincy St', false, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2, '', '', '22207');
INSERT INTO public.playgrounds VALUES (280, 'Monroe Park', NULL, '0 to 12', 2, 3, NULL, 2, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1330 S Monroe St<br>Chalkboard, community posting board for people to post community events. Dog friendly, but must have leash.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:52.688194', '2016-04-14 09:21:15.255082', 38.8574138199999979, -77.0926208300000013, '', 'http://www.novaplays.org/pgimages/Monroe.jpg', '1330 S Monroe St', false, 3, 3, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2, 1, 1, 3, 'chalkboard', '', '22204');
INSERT INTO public.playgrounds VALUES (285, 'Nottingham Elementary School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '5900 N Little Falls Road, Arlington VA 22207. The 2-5 playground is in front of the school, and the play features for older children are behind the school with the fields.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:54.066313', '2016-03-15 23:26:16.415163', 38.9006126900000027, -77.1509255999999937, '', 'http://www.novaplays.org/pgimages/Nottingham.jpg', '5900 N Little Falls Road', true, 3, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, '', '', '22207');
INSERT INTO public.playgrounds VALUES (282, 'Nauck Park', NULL, '2 to 5', 1, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 3, 1, NULL, NULL, NULL, NULL, NULL, '', '2551 19th St S, Arlington VA 22204. Limited play features: two swings, a slide and a spinning seat. Limited open space. Woods area is beyond the park fence. Seems very isolated on a dead end street. Clean, but no art or decorative features.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:53.345581', '2016-03-15 23:27:25.111221', 38.8549725100000032, -77.0821646600000037, '', 'http://www.novaplays.org/pgimages/Nauck.jpg', '2551 19th St S', false, 2, 3, 1, 1, 2, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (274, 'Lyon Village Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '1800 N Highland St<br>Street parking on perimeter of park. Clean and welcoming environment; interactive for multiple age groups. Tennis and basketball courts. Binoculars on playground and rock climbing wall. Wheelchair accessible.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:50.933683', '2016-04-14 09:23:58.762402', 38.8934911600000035, -77.0947260300000039, '', 'http://www.novaplays.org/pgimages/LyonVillage.jpg', '1800 N Highland St', false, 3, 3, 1, 1, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 1, 1, 1, 3, '', '', '22201');
INSERT INTO public.playgrounds VALUES (283, 'Nelly Custis Park', NULL, '0 to 5', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '701 24th St S, Arlington VA 22202. Great (paved) path for young kids to use wheeled toys. Play area mostly mulch and grass. Bus stop ~1 block away; another line ~4 blocks away. Easy to drive to, but only street parking available with permit. No decorative elements, but attractive park.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:53.696369', '2016-03-15 23:27:02.674987', 38.8525957599999998, -77.0577960100000041, '', 'http://www.novaplays.org/pgimages/NellyCustis.jpg', '701 24th St S', false, 3, 3, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 3, '', '', '22202');
INSERT INTO public.playgrounds VALUES (306, 'Vornado Crystal City', NULL, '0 to 5', 2, 2, NULL, 1, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2415 Crystal Drive, Arlington VA 22202. Open after 6 pm and on weekends.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:59.919845', '2016-03-15 23:15:54.868116', 38.8520876999999984, -77.0491331300000013, '', 'http://www.novaplays.org/pgimages/Vornado.jpg', '2451 Crystal Drive', false, 3, 3, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, '', '', '22202');
INSERT INTO public.playgrounds VALUES (301, 'Tuckahoe Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '2400 N Sycamore St, Arlington VA 22213. Recently renovated park; great space for kids to run and play and interesting new equipment, including a climbing web. Located next to Tuckahoe School. Fields used for soccer and baseball games. Close to East Falls Church Metro station and various bus lines.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:57.938754', '2016-03-16 00:10:47.616589', 38.8913128100000023, -77.1569560500000051, '', 'http://www.novaplays.org/pgimages/TuckahoePark.jpg', '2400 N Sycamore St', false, 3, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 3, '', '', '22213');
INSERT INTO public.playgrounds VALUES (289, 'Patrick Henry Elementary', NULL, '0 to 12', 2, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '701 S Highland St, Arlington VA 22204. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:55.19359', '2016-03-15 23:24:24.266611', 38.8657863700000021, -77.0888242500000018, '', 'http://www.novaplays.org/pgimages/PatrickHenry.jpg', '701 S Highland Street', true, 3, 3, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, '', '', '22204');
INSERT INTO public.playgrounds VALUES (296, 'Slater Park', NULL, '0 to 5', 2, 3, NULL, 2, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1837 N Culpeper St, Arlington VA 22207. Small playground but there is wooded area in back with picnic tables.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:56.925979', '2016-03-15 23:20:38.125848', 38.8932909700000025, -77.1237417800000031, '', 'http://www.novaplays.org/pgimages/Slater.jpg', '1837 N Culpeper St', false, 2, 3, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, '', '', '22207');
INSERT INTO public.playgrounds VALUES (304, 'Upton Hills Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '6060 Wilson Boulevard, Arlington VA 22205. The playground is down a path accessed from the main parking lot. Lots of woods for natural exploration. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:58.509826', '2016-03-15 23:17:35.954889', 38.8716318800000025, -77.1437849099999937, '', 'http://www.novaplays.org/pgimages/UptonHill.jpg', '6060 Wilson Boulevard', false, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 3, '', '', '22205');
INSERT INTO public.playgrounds VALUES (292, 'Quincy Park', NULL, '5 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 2, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1021 N Quincy St, Arlington VA 22201. Grills and picnic tables, volleyball court, baseball field, tennis courts. Surface parking lot shared with (adjacent) Central Library.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:56.091731', '2016-03-15 23:22:29.319685', 38.884532759999999, -77.1076284800000025, '', 'http://www.novaplays.org/pgimages/QuincyPark.jpg', '1021 N Quincy St', false, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22201');
INSERT INTO public.playgrounds VALUES (291, 'Powhatan Springs Skate Park', NULL, '5 to 12', 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 3, 2, NULL, NULL, NULL, NULL, NULL, '', 'Open sunrise to 9 pm Sunday through Thursday; sunrise to 10 pm Friday and Saturday.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:55.587718', '2016-03-15 23:23:22.430338', 38.8729080000000025, -77.1393560000000065, '', 'http://www.novaplays.org/pgimages/PowhatanSprings.jpg', '6020 Wilson Boulevard', false, 3, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, '', '', '22205');
INSERT INTO public.playgrounds VALUES (290, 'Penrose Park', NULL, '0 to 12', 2, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '2200 6th St S, Arlington VA 22204. Park is hidden in a residential area. Nice gazebo with picnic tables and a wooded picnic area with grills. On-street parking.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:55.374245', '2016-03-15 23:23:46.13361', 38.8686066799999992, -77.0826948699999974, '', 'http://www.novaplays.org/pgimages/Penrose.jpg', '2200 6th St S', false, 3, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, '', '', '22204');
INSERT INTO public.playgrounds VALUES (298, 'Thomas Jefferson Community Center Playground', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '3501 S 2nd St, Arlington VA 22204. Some of the playground surface is wheelchair accessible, but most is mulched. Tennis courts, soccer fields, adult fitness equipment also at park. Restrooms in the community center. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:57.331784', '2016-03-15 23:19:38.101025', 38.8696383300000008, -77.0961199500000021, '', 'http://www.novaplays.org/pgimages/ThomasJefferson.jpg', '3501 S 2nd St', false, 2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, '', '', '22204');
INSERT INTO public.playgrounds VALUES (295, 'Rosslyn Highlands Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1529 Wilson Boulevard, Arlington VA 22201.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:56.678488', '2016-03-15 23:21:18.568987', 38.8952806599999974, -77.0769057600000025, '', 'http://www.novaplays.org/pgimages/RosslynHighlands.jpg', '1529 Wilson Blvd', false, 2, 3, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, '', '', '22201');
INSERT INTO public.playgrounds VALUES (302, 'Tuckahoe Elementary School', NULL, '2 to 5', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '6500 26th St N, Arlington VA 22213. Playground is very close to the Tuckahoe Park; some of the features listed there are shared. Near the East Falls Church Metro station and buses.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:58.116189', '2016-03-15 23:18:26.107969', 38.8913128100000023, -77.1569560500000051, '', 'http://www.novaplays.org/pgimages/TuckahoeElem.jpg', '6550 26th St N', true, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 2, 2, 3, '', '', '22213');
INSERT INTO public.playgrounds VALUES (293, 'Randolph Elementary', NULL, '2 to 12', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1306 S Quincy St, Arlington VA 22204', NULL, NULL, NULL, '', '', '2015-05-10 22:06:56.269629', '2016-03-15 23:22:02.471461', 38.8565642100000019, -77.0980648299999984, '', 'http://www.novaplays.org/pgimages/Randolph.jpg', '1306 S Quincy Street', true, 3, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 1, '', '', '22204');
INSERT INTO public.playgrounds VALUES (299, 'Towers Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '801 S Scott St, Arlington VA 22204. Tennis court, volleyball court, gated dog park. Capital Bike Share stand close to park.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:57.518886', '2016-03-15 23:19:13.140037', 38.8668102999999974, -77.0774074400000018, '', 'http://www.novaplays.org/pgimages/Towers.jpg', '801 S Scott St', false, 3, 2, 1, 1, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (300, 'Troy Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2629 S Troy St, Arlington VA 22206. Water table toy for toddlers. Stream adjacent to park, but fenced off. Pleasant natural setting. Two basketball hoops, but not a full basketball court. Bus stops within walking distance, on bike route and bike share nearby.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:57.705761', '2016-03-15 23:18:50.625741', 38.8460190999999995, -77.0723257399999966, '', 'http://www.novaplays.org/pgimages/Troy.jpg', '2629 S Troy St', false, 2, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 3, '', '', '22206');
INSERT INTO public.playgrounds VALUES (303, 'Tyrol Hill Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '5101 7th Rd S, Arlington VA 22204. Limited street parking; a nice walk up a hill from the Arlington Mill Community Center.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:58.304641', '2016-03-15 23:18:03.997539', 38.8598923699999972, -77.1177272299999998, '', 'http://www.novaplays.org/pgimages/Tyrol.jpg', '5101 7th Rd S', false, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 3, 'Sandbox looks questionable- I took a picture.  Basketball Court  Volleyball Court', '', '22204');
INSERT INTO public.playgrounds VALUES (310, 'Woodmont Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2422 N Fillmore Street, Arlington, VA 22207. Paths are wheelchair accessible but no specific features for children with physical disabilities. No fence separating playground from parking lot. Easily accessible by car.', NULL, NULL, NULL, '', '', '2015-05-10 22:07:01.637039', '2016-03-15 23:07:36.990138', 38.9023451999999992, -77.097739709999999, '', 'http://www.novaplays.org/pgimages/Woodmont.png', '2422 N Fillmore St', false, 3, 2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (309, 'Woodlawn Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '1325 N Buchanan Street, Arlington VA 22207. The park is not immediately visible from the street; walk down a short path between two houses to get there.', NULL, NULL, NULL, '', '', '2015-05-10 22:07:01.345045', '2016-03-15 23:13:42.542083', 38.8868860500000011, -77.1207220799999931, '', 'http://www.novaplays.org/pgimages/Woodlawn.jpg', '1325 N Buchanan St', false, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 3, '', '', '22207');
INSERT INTO public.playgrounds VALUES (308, 'Westover Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1001 N Kennebec St, Arlington VA 22205. Pleasant and welcoming facility. Baseball field, volleyball court, basketball court, soccer field, bike trail, and one swing for children with disabilities. Small parking lot.', NULL, NULL, NULL, '', '', '2015-05-10 22:07:00.490498', '2016-03-15 23:15:08.564081', 38.8801941400000004, -77.1357041699999968, '', 'http://www.novaplays.org/pgimages/Westover.jpg', '1001 N Kennebec St', false, 3, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22205');
INSERT INTO public.playgrounds VALUES (307, 'Walter Reed Playground', NULL, '2 to 5', 1, 3, NULL, 3, 3, 1, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '2909 16th St S, Arlington VA 22204. Very small playground, without the traditional slide or swings. Geared toward kids who like to climb and balance. The sandbox is quite nice, with a turtle sculpture in the center. Adjacent to community center, off Walter Reed Drive. Ample parking in a lot. Restrooms in the community center. ', NULL, NULL, NULL, '', '', '2015-05-10 22:07:00.144388', '2016-03-15 23:15:35.2757', 38.8579347499999983, -77.0862320100000034, '', 'http://www.novaplays.org/pgimages/WalterReed.jpg', '2909 16th Street S', false, 3, 1, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 1, 1, 1, 1, 1, '', '', '22204');
INSERT INTO public.playgrounds VALUES (275, 'Madison Community Center Park/Fort Ethan Allen Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', 'Blacktop for wheeled toys plus a huge field for ball play.  Separate play areas for younger and older children. There are several benches and picnic tables. Restrooms are in the community center. Community dog park (well-fenced) next to the playground. Fort Ethan Allen Park is across Stafford street; it has a gazebo and historical signs.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:51.181687', '2016-03-15 23:30:51.810602', 38.9232514099999989, -77.1237129299999964, '', 'http://www.novaplays.org/pgimages/FortEthan.jpg', '3829 N Stafford St', false, 2, 3, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2, '', '', '22207');
INSERT INTO public.playgrounds VALUES (297, 'Taylor Elementary School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2600 N Stuart St, Arlington VA 22207. There are three separate playground areas including one behind the school, which is not visible from the street. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:57.125871', '2016-03-15 23:20:14.451228', 38.9067854700000026, -77.1136496400000055, '', 'http://www.novaplays.org/pgimages/Taylor.jpg', '2600 N Stuart Street', true, 3, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, '', '', '22207');
INSERT INTO public.playgrounds VALUES (294, 'Rocky Run Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1109 N Barton St, Arlington VA 22201. Renovated in 2014, Rocky Run offers fun features for children of all ages. Large and small web climbing features. Within walking distance of the Courthouse Metro Station, and several bus lines run along N. Barton Street. Street parking available.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:56.456743', '2016-03-15 23:21:40.182505', 38.8865398800000008, -77.0880302599999965, '', 'http://www.novaplays.org/pgimages/RockyRun.jpg', '1109 N Barton St', false, 3, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 1, '', '', '22201');
INSERT INTO public.playgrounds VALUES (248, 'Drew Park/Drew School', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '3500 23rd South<br>School park features include playhouse, garden plots, marked pavement for games, and many more accessible spaces for children with disabilities.  Baseball field. No woods, but nice open spaces and hill with trees.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:39.788188', '2016-03-15 23:44:37.804811', 38.8481816600000016, -77.0867985099999942, '', 'http://www.novaplays.org/pgimages/Drew.jpg', '3500 23rd South', true, 3, 3, 3, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 2, 2, 1, 2, 3, '', 'more at school park than park itself', '22206');
INSERT INTO public.playgrounds VALUES (287, 'Oakridge Elementary School', NULL, '2 to 12', 1, 2, NULL, 2, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1414 24th St S, Arlington VA 22202. Two play areas: one for young children 2-5, 5 - 12 and then a play area for older children. Both play areas are not viewable at the same time, so caregivers must choose which area to stay in. Both play areas are on mulch, so are not wheelchair friendly. Outside school hours, playground is typically not crowded.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:54.815292', '2015-11-10 20:44:43.61678', 38.8505108800000016, -77.0702403100000026, '', '', '1414 24th Street S', true, 3, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, '', '', '22202');
INSERT INTO public.playgrounds VALUES (255, 'Fort Myer Heights Park', NULL, '2 to 12', 1, 3, NULL, 2, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1400 Fort Myer Dr<br>There is a bus stop right outside it but it is far from a nearby metro. Located immediately above route 50 (Arlington Boulevard). There is a basketball court but you have to go up steep steps (that are not enclosed by a gate) to get to it. The park space is on hard surface or fake grass so it would be easy for a wheelchair to navigate, but the space is tight between play structures. Next to the sand box is a water fountain for play with water and sand. The two play structures are fun because the slides are tree trunks so the slide is a tunnel.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:42.126867', '2016-04-14 09:33:59.471234', 38.8907176800000016, -77.0750816000000043, '', 'http://www.novaplays.org/pgimages/FortMyer.jpg', '1400 Fort Myer Dr', false, 3, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2, 1, 1, 2, 1, 2, '', '', '22201');
INSERT INTO public.playgrounds VALUES (230, 'Barcroft Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '4200 S Four Mile Run Dr<br>Water fountains are seasonal. Barcroft in the Woods (behind baseball fields) has swings. The age 2-5 area is on wood chips, but has a few features for children with disabilities. Other features include soccer field, backboard for ball-throwing, many baseball fields.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:35.841767', '2016-03-15 23:51:27.779025', 38.8505482799999982, -77.1019018700000061, '', 'http://www.novaplays.org/pgimages/BarcroftPark.jpg', '4200 S Four Mile Run Dr', false, 3, 2, 2, 1, 2, 2, 1, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, '', '', '22206');
INSERT INTO public.playgrounds VALUES (227, 'Aurora Hills Community and Senior center', NULL, '2 to 12', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '735 18th Street S<br>Parking is available. Near Pentagon City metro, many bus routes. Restrooms open March to October. Restrooms also in library when the library is open. Adjacent to Virginia Highlands Park (spray park, tennis, basketball, open fields), a community center, and a library. Large sidewalk next to park where kids can ride bikes. Surfaces in this park are mulched (difficult for wheelchairs). ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:35.271758', '2016-03-15 23:52:39.085932', 38.8574458100000015, -77.0590110600000031, '', 'http://www.novaplays.org/pgimages/AuroraHills.jpg', '735 18th Street S', false, 3, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 3, '', '', '22202');
INSERT INTO public.playgrounds VALUES (247, 'Douglas Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1718 S Quincy St<br>Sides of park connected by bridge. Dog friendly. Paths to take walks/hiking paths. Parking lot restricted from 9pm-6am.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:39.600599', '2016-03-15 23:44:57.738584', 38.8527326000000031, -77.0964501600000034, '', 'http://www.novaplays.org/pgimages/Douglas.jpg', '1718 S Quincy St', false, 3, 2, 1, 2, 2, 1, 1, 1, 1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 2, 2, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (240, 'Carver Community Center/Hoffman Boston Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '1415 S Queen St<br>Huge playground with multiple attractions, including swing for children with disabilities, tennis courts, basketball court, soccer field. Community Center open 6pm-9pm. No dogs allowed. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:38.168715', '2016-03-15 23:47:35.954324', 38.8615337600000004, -77.0718405500000046, '', 'http://www.novaplays.org/pgimages/HoffmanBoston.jpg', '1415 S Queen St', true, 3, 3, 3, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 1, 3, '', '', '22204');
INSERT INTO public.playgrounds VALUES (249, 'Eads Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '2730 S Eads St<br>Note: Path on relatively steep hill. Playground is fenced and surfaces are mulched. Open/soccer field fenced on three sides. Street parking only.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:40.022796', '2016-03-15 23:44:20.617779', 38.8481356400000024, -77.0552017299999932, '', 'http://www.novaplays.org/pgimages/Eads.jpg', '2730 S Eads St', false, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 3, '', '', '22202');
INSERT INTO public.playgrounds VALUES (232, 'Benjamin Banneker Park', NULL, '2 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '6620 N 18th St<br>Playground has limited play area and is not fenced. Lots of room to run around and interact with natural surroundings in a wooded area. Metro is very close to the area; parking is very limited because you must park on the side of the street beside the park. It is within a neighborhood and the W&OD trail runs through the park.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:36.249507', '2016-03-15 23:50:39.897364', 38.8831833200000005, -77.1579594699999944, '', 'http://www.novaplays.org/pgimages/BenjaminBanneker.jpg', '6620 N 18th St', false, 2, 1, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, '', '', '22205');
INSERT INTO public.playgrounds VALUES (305, 'Virginia Highlands Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1600 S Hayes Street, Arlington VA 22202. Large park with a fenced playground. Spray ground is open every day, Memorial Day through Labor Day, but is not fenced. Nice restrooms. The fields and ball court surrounding the park were under construction at the time of survey. Close to Pentagon City Metro, plenty of parking (except for during construction right now)', NULL, NULL, NULL, '', '', '2015-05-10 22:06:59.477989', '2015-11-10 20:23:38.681133', 38.8584014999999994, -77.0598723800000016, '', '', '1600 S Hayes St', false, 2, 2, 1, 1, 2, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 1, '', '', '22202');
INSERT INTO public.playgrounds VALUES (270, 'Lacey Woods Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 3, 2, NULL, NULL, NULL, NULL, NULL, '', '1200 N George Mason Dr<br>Pavilion with seating next to the play structures. Lots of seating with line of sight supervision of kids. The playground is labeled for the various age groups. Neighborhood park accessible by walking or car (street parking on perimeter of park).', NULL, NULL, NULL, '', '', '2015-05-10 22:06:49.550769', '2016-04-14 09:26:16.492488', 38.8845637599999989, -77.1267641700000013, '', 'http://www.novaplays.org/pgimages/LaceyWoods.jpg', '1200 N George Mason Dr', false, 3, 2, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, '', '', '22205');
INSERT INTO public.playgrounds VALUES (288, 'Parkhurst Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '5820 20th Rd N, Arlington VA 22205. Playground has a nice layout, and the areas are divided by age-appropriate toys and activities. The woods help the setting seem less urban. Pavilion available with shade and seating. Restrooms closed during winter.', NULL, NULL, NULL, '', '', '2015-05-10 22:06:55.017511', '2016-03-15 23:24:45.501878', 38.88934493, -77.1417904700000037, '', 'http://www.novaplays.org/pgimages/Parkhurst.jpg', '5820 20th Rd N', false, 3, 3, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, '', '', '22205');
INSERT INTO public.playgrounds VALUES (231, 'Barrett Elementary School', NULL, '2 to 12', 1, 2, NULL, 3, 3, 1, NULL, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, '', '4401 N Henderson Road<br>There are two play structures in this playground. The one for younger kids is behind the trailers, and includes a community garden and a cool mosaic. There is also a softball field with a seating area. Lubber Run Community Center is steps away, and bathrooms are available there during open hours. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:36.019978', '2016-03-15 23:51:05.673343', 38.8728133500000013, -77.1120146599999998, '', 'http://www.novaplays.org/pgimages/Barrett.jpg', '4401 N Henderson Road', true, 3, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 1, 2, 1, '', '', '22203');
INSERT INTO public.playgrounds VALUES (277, 'Maury Park', NULL, '2 to 12', 1, 3, NULL, 3, 3, 3, NULL, NULL, NULL, 3, 3, 1, NULL, NULL, NULL, NULL, NULL, '', '3550 Wilson Blvd<br>Play structure is a piece of interactive sculpture created by a German artist to match the same one he created for Aachen, Arlington''s German sister city.  It''s a play ship with a spinning bottom and kaleidoscope "telescopes" on the top level. It''s located at the southwest corner of the Arlington Arts Center lawn. Surface is molded foam.  One steering wheel element is accessible from the ground. There are temporary art installments on the lawn of the Arlington Arts Center. Tennis courts; two large grassy areas. Parking lot (~25 spaces) for Center and park visitors. Two blocks from Virginia Square Metro Station; bus stop on Wilson Blvd. Restrooms in the Arts Center. ', NULL, NULL, NULL, '', '', '2015-05-10 22:06:51.899336', '2016-04-14 09:23:00.55062', 38.8817351599999981, -77.1020934000000011, '', 'http://www.novaplays.org/pgimages/Maury.jpg', '3550 Wilson Blvd', false, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 3, '', '', '22201');
INSERT INTO public.playgrounds VALUES (253, 'Fort Barnard Park', NULL, '0 to 12', 2, 3, NULL, 3, 3, 2, NULL, NULL, NULL, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, '', '2101 S Pollard St<br>Visible from Walter Reed Dr. Parking is limited to neighborhood streets. Very attractive, new playground. Loads of benches. There is a nice Gazebo for picnicking and shade. Surfaces are soft (not mulch), which would easily be navigable by a wheelchair; large swing. Water fountain is only on seasonally and is adjacent to the basketball court (which has lights for night play).', NULL, NULL, NULL, '', '', '2015-05-10 22:06:41.647917', '2016-04-14 09:34:25.079093', 38.8491985400000033, -77.0924762399999963, '', 'http://www.novaplays.org/pgimages/FortBarnard.jpg', '2101 S Pollard St', false, 3, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, '', '', '22204');


--
-- Name: playgrounds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alexandriaplays
--

SELECT pg_catalog.setval('public.playgrounds_id_seq', 310, true);


--
-- Data for Name: rails_admin_histories; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--



--
-- Name: rails_admin_histories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alexandriaplays
--

SELECT pg_catalog.setval('public.rails_admin_histories_id_seq', 1, false);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--

INSERT INTO public.schema_migrations VALUES ('20120908185222');
INSERT INTO public.schema_migrations VALUES ('20120908234022');
INSERT INTO public.schema_migrations VALUES ('20120909004846');
INSERT INTO public.schema_migrations VALUES ('20120909020834');
INSERT INTO public.schema_migrations VALUES ('20120928002207');
INSERT INTO public.schema_migrations VALUES ('20121008180237');
INSERT INTO public.schema_migrations VALUES ('20121008180607');
INSERT INTO public.schema_migrations VALUES ('20121008180901');
INSERT INTO public.schema_migrations VALUES ('20121117203219');
INSERT INTO public.schema_migrations VALUES ('20121117204035');
INSERT INTO public.schema_migrations VALUES ('20121118171458');
INSERT INTO public.schema_migrations VALUES ('20121124235629');
INSERT INTO public.schema_migrations VALUES ('20120908234021');
INSERT INTO public.schema_migrations VALUES ('20140607180353');
INSERT INTO public.schema_migrations VALUES ('20140907174927');
INSERT INTO public.schema_migrations VALUES ('20150503152853');
INSERT INTO public.schema_migrations VALUES ('20150503203427');
INSERT INTO public.schema_migrations VALUES ('20150503204002');
INSERT INTO public.schema_migrations VALUES ('20150503204243');
INSERT INTO public.schema_migrations VALUES ('20150503205822');
INSERT INTO public.schema_migrations VALUES ('20150503211554');
INSERT INTO public.schema_migrations VALUES ('20150503211742');
INSERT INTO public.schema_migrations VALUES ('20150510213310');


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: alexandriaplays
--

INSERT INTO public.users VALUES (2, 'ckissell@thereadingconnection.org', '$2a$10$m3OnXme7RajwoYqepFxyrumCKN183xC/WpQ79538xZ5ePtO0pjJr6', NULL, NULL, NULL, 2, '2014-06-11 02:02:24.868027', '2014-06-11 01:39:52.574082', '68.50.75.171', '68.50.75.171', '2014-06-11 01:00:45.563426', '2014-06-11 02:02:24.869108');
INSERT INTO public.users VALUES (3, 'ddombrow@gmail.com', '$2a$10$dVU29d5gxj.OCeaC8Dj8PO8yDFBQuui2/L0wM7c7pQncxM5kKcbay', NULL, NULL, NULL, 1, '2014-06-11 02:20:14.76226', '2014-06-11 02:20:14.76226', '68.50.75.171', '', '2014-06-11 02:18:42.996078', '2014-06-11 02:20:14.762708');
INSERT INTO public.users VALUES (4, 'kerrinepstein@gmail.com', '$2a$10$MuAzfqlbLGy0o57ukbVZwOUAxBgaVbXz/PFAzFz6WNdxWwDHQjYy.', '0a670eaaf480952205d16579dda70538a5e04036b4b697722bcfab0b1aeadc72', '2015-04-16 23:11:00', NULL, 10, '2015-04-16 23:14:37.671583', '2015-03-27 20:20:00', '198.181.231.228', '70.88.137.89', '2014-10-23 23:05:58.765937', '2015-04-16 23:14:37.672191');
INSERT INTO public.users VALUES (8, 'yully0120@gmail.com', '$2a$10$D8AsVC3Lo/tB27WQ16DEfe1HL1haU7.lUqw72D1KT7J6PNmKEbBSW', NULL, '2015-08-31 00:00:00', NULL, 1, '2015-04-17 00:23:50.094403', '2015-04-17 00:23:50.094403', '198.181.231.228', '', '2015-04-17 00:20:58.192405', '2015-04-17 00:23:50.095824');
INSERT INTO public.users VALUES (6, 'mattva01@gmail.com', '$2a$10$odj8nO9R/8Cc9pBVt.yN4.6at8eWTyNcTaX8UXry7xlM/Y5AjYHGy', NULL, NULL, NULL, 0, NULL, NULL, '', '', '2015-02-21 15:47:20.796355', '2015-02-21 15:47:20.796355');
INSERT INTO public.users VALUES (5, 'moniquemartineau@gmail.com', '$2a$10$d5trgbaZ96FPRvromqXZQe5eWQ483K8M1nO7/3eDtH/1KrkR9EGdu', NULL, NULL, '2016-04-17 12:41:22.727629', 31, '2016-04-17 12:41:22.755317', '2016-04-14 09:13:42.075307', '74.96.237.134', '74.96.177.70', '2014-12-08 21:50:11.692025', '2016-04-17 12:41:22.756219');
INSERT INTO public.users VALUES (1, 'michelle@codeforamerica.org', '$2a$10$3D3vJ6POGer50iOozRgYLuXBpGKij811KBrNocc60ezk6mLrRJ0Km', NULL, NULL, NULL, 21, '2016-12-16 01:32:38.405622', '2016-03-15 23:05:58.976756', '158.59.146.11', '98.231.182.145', '2014-06-07 17:40:44.753848', '2016-12-16 01:32:38.406892');
INSERT INTO public.users VALUES (30, 'jzthegreat@gmail.com', '$2a$10$UmGDctlhdDhDa3oxARblruPtoSZt3YI7BE5SHcBxK0BulgO7Eov/G', NULL, NULL, NULL, 0, NULL, NULL, '', '', '2016-12-16 01:36:20.615806', '2016-12-16 01:36:20.615806');
INSERT INTO public.users VALUES (31, 'merylgibbs@gmail.com', '$2a$10$dgaF9739YmpZrGpi1fgpguVXYv54JiI6dpAy42Xbp1gTH.v43QM6S', NULL, NULL, NULL, 0, NULL, NULL, '', '', '2016-12-16 01:37:22.718801', '2016-12-16 01:37:22.718801');


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alexandriaplays
--

SELECT pg_catalog.setval('public.users_id_seq', 31, true);


--
-- Name: aliases_pkey; Type: CONSTRAINT; Schema: public; Owner: alexandriaplays; Tablespace: 
--

ALTER TABLE ONLY public.aliases
    ADD CONSTRAINT aliases_pkey PRIMARY KEY (id);


--
-- Name: criteriakeys_pkey; Type: CONSTRAINT; Schema: public; Owner: alexandriaplays; Tablespace: 
--

ALTER TABLE ONLY public.criteriakeys
    ADD CONSTRAINT criteriakeys_pkey PRIMARY KEY (id);


--
-- Name: playgrounds_pkey; Type: CONSTRAINT; Schema: public; Owner: alexandriaplays; Tablespace: 
--

ALTER TABLE ONLY public.playgrounds
    ADD CONSTRAINT playgrounds_pkey PRIMARY KEY (id);


--
-- Name: rails_admin_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: alexandriaplays; Tablespace: 
--

ALTER TABLE ONLY public.rails_admin_histories
    ADD CONSTRAINT rails_admin_histories_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: alexandriaplays; Tablespace: 
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_rails_admin_histories; Type: INDEX; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE INDEX index_rails_admin_histories ON public.rails_admin_histories USING btree (item, "table", month, year);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: alexandriaplays; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

