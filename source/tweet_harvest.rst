..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _tweet_harvest:

Harvesting Twitter Data with Python
===================================

.. index:: twitter, tweets, twitter data 

Twitter Data
------------

`Twitter <https://en.wikipedia.org/wiki/Twitter>`__ is a
`social networking service
<https://en.wikipedia.org/wiki/Social_networking_service>`__ used by millions
of people who produce hundreds of millions of **tweets** each day.  Twitter
provides public access to this vast quatity of data through their
`REST APIs <https://dev.twitter.com/rest/public>`_.

Setup
-----

`Tweepy <http://tweepy.readthedocs.org>`__ is a python module for interacting
with the Twitter APIs.  There is an Ubuntu 17.10 Python 3 package for Tweepy,
which can be installed with::

    $ sudo apt install python3-tweepy

