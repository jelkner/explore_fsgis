import json

f = open('block_groups_inserts.sql', 'w')

with open('../Data/acs5.json') as json_data:
    d = json.load(json_data)
    s = 'INSERT INTO block_groups\n'
    s += 'Values ({}, {}, {});\n'
    for row in d[1:]:
        f.write(s.format(row[3], row[4], 0 if not row[0] else row[0]))

f.close()
