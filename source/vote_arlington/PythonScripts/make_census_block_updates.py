import json

f = open('census_block_updates.sql', 'w')

with open('../Data/sf1.json') as json_data:
    d = json.load(json_data)
    s = 'UPDATE census_blocks\n'
    s += 'SET total_pop = {}\n'
    s += "WHERE tract = '{}'\n"
    s += "AND block = '{}';\n"
    for row in d[1:]:
        f.write(s.format(int(row[0]), row[-2], row[-1]))

f.close()
