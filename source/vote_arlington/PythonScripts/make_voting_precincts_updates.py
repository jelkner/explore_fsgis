import csv 

f = open('voting_precincts_updates.sql', 'w')

with open('../Data/arl_nov_2010_voter_data.csv') as csvfile:
    csvr = csv.reader(csvfile)
    h = csvr.__next__() 
    s = 'UPDATE voting_precincts\n'
    s += 'SET vote_age_pop = {}, voter_turnout = {}\n'
    s += "WHERE precinct_num = '{}';\n"
    # print(h[0], h[1], h[14], h[-1])
    for r in csvr:
        # print('{} {} {} {}'.format(r[0][-2:], r[1], r[14], r[-1]))
        f.write(s.format(r[14], r[-1], r[0][-2:]))

f.close()
