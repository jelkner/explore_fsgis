import pandas as pd
from sqlalchemy import create_engine

connect_str = 'postgresql://jelkner:passwd@localhost:5432/our_arlington'
engine = create_engine(connect_str)
d = pd.read_sql_table('precinct_data', con=engine, index_col='precinct_num')
d.to_pickle('precinct_data.pkl')
