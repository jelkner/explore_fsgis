import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statistics as stat
import thinkstats2 as ts
import thinkplot as tp
import scipy.stats.stats as scistat


# Define a helper function for printing stats
def report_stats(statlist):
    for raw_message, value, value_format in statlist:
        message = raw_message + value_format
        print(message.format(value))
    print()


# Load the data and sort by income
data = pd.read_pickle('Data/precinct_data.pkl')
sd = data.sort_values(by=['income_per_cap'])

# Load the two Series from the DataFrame
income = sd['income_per_cap']
voting = sd['vote_rate']

# Generate scatter plot and least squares line
plt.rcdefaults()
inter, slope = ts.LeastSquares(income, voting)
fit_income, fit_voting = ts.FitLine(income, inter, slope)
tp.Scatter(income, voting)
tp.Plot(fit_income, fit_voting)
tp.Show(title='Voting Rate vs Income in Arlington, VA 2010 Election',
        xlabel='Income per Capita ($)',
        ylabel='Voting Participation Rate (%)')

# Report descriptive statistics on the data
ds = [('Mean income: $', round(stat.mean(income)), '{}'),
      ('Mean voting participation rate: ', round(stat.mean(voting)), '{}%'),
      ('Median income: ', stat.median(income), '${}'),
      ('Median voting participation rate: ', stat.median(voting), '{}%'),
      ('Income sample standard deviation: ', round(stat.stdev(income)), '${}'),
      ('Voting participation rate sample standard deviation: ',
       stat.stdev(voting), '{:0.2f}%')]

report_stats(ds)

# Investigate correlation between income and voting participation
covar = np.cov(income, voting)[0][1]
rho, p_val = scistat.pearsonr(income, voting)

cs = [('Covariance between income and voting is: ', covar, '{:0.2f}'),
      ("The Pearson's correlation between income and voting is: ", rho,
       '{:0.2f}'),
      ('The p-value for this correlation is: ', p_val, '{:0.2e}')]

report_stats(cs)
