SELECT
  vp.precinct_num,
  vp.precinct_name,
  cb.tract,
  cb.block,
  cb.total_pop
FROM
  voting_precincts AS vp
LEFT JOIN
  census_blocks AS cb
ON
  ST_Within(cb.geom, vp.geom)
ORDER BY
  vp.precinct_num;
