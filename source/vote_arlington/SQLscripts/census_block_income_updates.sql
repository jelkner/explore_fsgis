SELECT tract || substring(block from 1 for 1) FROM census_blocks AS cbnum,
       tract || block_group FROM block_groups AS bgnum;
