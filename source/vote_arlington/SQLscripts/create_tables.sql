CREATE TABLE census_blocks ( 
    gid integer,
    tract char(6),
    block char(4),
    total_pop integer NOT NULL DEFAULT 0,
    geom geometry(MultiPolygon,4269),
    PRIMARY KEY(gid)
);
CREATE TABLE voting_precincts (
    gid integer,
    precinct_num char(2),
    precinct_name text,
    vote_age_pop integer NOT NULL DEFAULT 0,
    voter_turnout integer NOT NULL DEFAULT 0,
    geom geometry(MultiPolygon,4269),
    PRIMARY KEY(gid)
);
CREATE TABLE block_groups (
    tract char(6),
    block_group char(1),
    income_percap integer NOT NULL DEFAULT 0,
    PRIMARY KEY(tract, block_group)
);
