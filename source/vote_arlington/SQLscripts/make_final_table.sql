CREATE TABLE precinct_data
AS (SELECT
      vp.precinct_num,
      vp.precinct_name,
      SUM(bg.income_percap * cb.total_pop) / SUM(cb.total_pop)
      AS income_per_cap,
      ((vp.voter_turnout * 100)::numeric / vp.vote_age_pop)::numeric(4, 2)
      AS vote_rate
    FROM
      census_blocks AS cb
    LEFT JOIN
      block_groups AS bg
    ON
      cb.tract || substring(cb.block from 1 for 1) = bg.tract || bg.block_group
    LEFT JOIN
      voting_precincts as vp
    ON
      ST_Within(cb.geom,vp.geom)
    GROUP BY
      vp.precinct_num,
      vp.precinct_name,
      vote_rate
    ORDER BY
      vp.precinct_num);
