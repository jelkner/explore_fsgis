INSERT INTO census_blocks (gid, tract, block, geom)
SELECT gid, tractce10, blockce10, geom
FROM raw_census_blocks;
INSERT INTO voting_precincts (gid, precinct_num, precinct_name, geom)
SELECT gid, substring(vtdst10 from 2 for 2), name10, geom
FROM raw_voting_precincts;
