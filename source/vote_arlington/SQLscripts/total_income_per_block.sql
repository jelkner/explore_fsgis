SELECT
  cb.tract, cb.block, bg.income_percap * cb.total_pop AS total_income
FROM
  census_blocks AS cb
LEFT JOIN
  block_groups AS bg
ON
  cb.tract || substring(cb.block from 1 for 1) = bg.tract || bg.block_group
ORDER BY
  cb.tract
