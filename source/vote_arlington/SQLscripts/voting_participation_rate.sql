SELECT precinct_num, precinct_name,
((voter_turnout * 100)::numeric / vote_age_pop)::numeric(4, 2) AS vote_rate
FROM voting_precincts ORDER BY precinct_num;
