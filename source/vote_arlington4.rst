..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _vote_arlington_4:

Vote Arlington 4: Analyzing the Data
====================================

`PostGIS <https://en.wikipedia.org/wiki/PostGIS>`__ proved to be an effective
tool in manipulating and processing data into the format needed for analysis.
To do that analaysis, however, I'll turn to `Python
<https://en.wikipedia.org/wiki/Python_(programming_language)>`__ tools.


From PostGIS to Pandas
----------------------

The first step is to get the final table at the end of :ref:`vote_arlington_3`
out of the database and into a `pandas
<https://en.wikipedia.org/wiki/Pandas_(software)>`__ `DataFrame
<http://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.html>`__.
A bit of `SQLAlchemy <https://en.wikipedia.org/wiki/SQLAlchemy>`__ will make
this a snap:

.. literalinclude:: vote_arlington/PythonScripts/postgis_data2pandas.py
   :language: python 

.. note::

    ``passwd`` here was substituted for the actual database password.

and now I can use `NumPy <https://en.wikipedia.org/wiki/NumPy>`__, `SciPy
<https://en.wikipedia.org/wiki/SciPy>`__ and `Allen Downey's
<https://en.wikipedia.org/wiki/Allen_B._Downey>`__ `thinkstats2.py
<https://github.com/AllenDowney/ThinkStats2/blob/master/code/thinkstats2.py>`__
module in addition to pandas to compute some useful statistics on the data:

.. literalinclude:: vote_arlington/PythonScripts/report_stats.py
   :language: python 

which generates the plot:

.. image:: illustrations/vote_arlington4/scatter_plot.png
   :alt: Scatter Plot and Least Squares Line 

and the following statistics::

    Mean income: $60563
    Mean voting participation rate: 36.0%
    Median income: $61699
    Median voting participation rate: 33.83%
    Income sample standard deviation: $17310
    Voting participation rate sample standard deviation: 12.07%

    Covariance between income and voting is: 107588.89
    The Pearson's correlation between income and voting is: 0.52
    The p-value for this correlation is: 1.10e-04


Results
-------

To reach a conclusion from this process, I defined the null and alternative
hypotheses thus::

    :math:$H_0$ = There is no correlation between income and voting rate
    :math:$H_a$ = There is a positive correlation between income and voting rate

The `Pearson's correlation
<https://en.wikipedia.org/wiki/Pearson_correlation_coefficient>`__ between
income and voting rates in the sample of 51 voting precincts in Arlington
County, Virginia in 2010 is 0.52, a borderline `high degree of correlation
<https://statistics.laerd.com/statistical-guides/pearson-correlation-coefficient-statistical-guide.php>`_.

The `p-value <https://en.wikipedia.org/wiki/P-value>`_ for this correlation is
0.00011, which is well within the statistically `highly significant range
<https://www.statsdirect.com/help/basics/p_values.htm>`_. The null hypothesis
can thus be rejected, and it can be concluded that a correlation between income
and voting rate is likely.


Conclusion
----------

I began this investigation began with a hypothesis that I considered likely to
hold, that a positive correlation would be found between income and voting
participation. In the process of doing the investigation, obtaining and
processing the data turned out to be a far greater challenge than the
statistical analysis itself.  It took a few weeks of investigation (due in
large part, no doubt, to my inexperience with this kind of investigation) to
find the data sources I needed. It then took an even longer period of time to
figure out how to process the data into the format required for analysis. Once
that was accomplished, it was a straightforward process to analyze the data and
draw a conclusion from it. 
