..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with no Invariant Sections, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".

.. _vote_arlington_5:

Vote Arlington 5: Extending the Analysis North and South 
=========================================================

Even a cursory glance at the :ref:`choropleth maps <choropleth_maps>` in
:ref:`vote_arlington_3` reveals an apparent difference in both income and
voting participation rates between `North Arlington
<https://en.wikipedia.org/wiki/North_Arlington,_Virginia>`__ and
`South Arlington <https://en.wikipedia.org/wiki/South_Arlington,_Virginia>`__.
As a final step in this exploration, I'll extend the previous analysis to take
a statistical look at the difference between North and South Arlington.

The geographic boundary between North and South Arlington is `Arlington
Boulevard <https://en.wikipedia.org/wiki/Arlington_Boulevard>`__, so I'll want
to load a `linestring <https://en.wikipedia.org/wiki/Polygonal_chain>`__
representation of Arlington Blvd. into the database to select voting precincts
to the north and south of it.

I found the data I need in the `Roads
<http://gisdata-arlgis.opendata.arcgis.com/datasets/roads/data>`__ data on the
`Arlington County GIS Open Data <http://gisdata-arlgis.opendata.arcgis.com/>`__
After unzipping ``Roads.zip`` I ran::

    $ shp2pgsql -s 4269 Roads.shp roads | psql -d our_arlington

Connecting to the database from QGIS shows the layer.

.. image:: illustrations/vote_arlington5/arlington_roads.png
   :alt: Arlington Roads layer 
